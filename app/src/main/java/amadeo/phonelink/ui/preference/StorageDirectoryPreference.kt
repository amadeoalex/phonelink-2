package amadeo.phonelink.ui.preference

import android.content.Context
import android.util.AttributeSet
import androidx.preference.Preference

class StorageDirectoryPreference(context: Context?, attrs: AttributeSet?) : Preference(context, attrs) {

    constructor(context: Context?) : this(context, null)

}
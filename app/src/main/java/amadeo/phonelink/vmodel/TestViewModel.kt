package amadeo.phonelink.vmodel

import amadeo.phonelink.device.DeviceManager
import amadeo.phonelink.service.ForegroundService
import amadeo.phonelink.service.PhoneLinkService
import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder

class TestViewModel(application: Application) : NotifyingAndroidServiceViewModel(application,ForegroundService::class.java) {
    lateinit var deviceManager: DeviceManager

    override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
        val service = (binder as ForegroundService.PhoneLinkBinder).getService()

        deviceManager = service.deviceManager

        setViewModelReady()
    }
}
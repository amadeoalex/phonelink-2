package amadeo.phonelink.ui.fragment

import amadeo.phonelink.R
import amadeo.phonelink.feature.Feature
import amadeo.phonelink.ui.preference.SwitchSettingsPreference
import amadeo.phonelink.vmodel.DevicesViewModel
import amadeo.phonelink.vmodel.NotifyingAndroidServiceViewModel
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider

class DeviceFeaturesFragment : SettingsFragment(), NotifyingAndroidServiceViewModel.OnViewModelReadyListener {

    private lateinit var deviceId: String
    private lateinit var viewModel: DevicesViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val argumentsBundle = arguments
        require(argumentsBundle != null && argumentsBundle.containsKey(DeviceSettingsFragment.ARG_DEVICE_ID)) { throw IllegalArgumentException("missing device id argument")}
        deviceId = argumentsBundle.getString(DeviceSettingsFragment.ARG_DEVICE_ID)!!
        viewModel = ViewModelProvider(activity!!)[DevicesViewModel::class.java]

        title = getString(R.string.device_features, deviceId)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        Toast.makeText(context, "devicef id " + arguments?.getString(DeviceSettingsFragment.ARG_DEVICE_ID), Toast.LENGTH_SHORT).show()
        setPreferencesFromResource(R.xml.preferences_device_features, rootKey)

        viewModel.setOnReadyListener(this)

//        preferenceScreen.addPreference(Preference(preferenceManager.context).apply {
//            title = "Test1"
//            summary = "Summary1"
//        })
//        preferenceScreen.addPreference(Preference(preferenceManager.context).apply {
//            title = "Test2"
//            summary = "Summary2"
//        })
//        preferenceScreen.addPreference(Preference(preferenceManager.context).apply {
//            title = "Test3"
//            summary = "Summary3"
//        })
    }

    override fun onViewModelReady() {
        val preferenceScreen = preferenceScreen
        val preferenceContext = preferenceManager.context

        val device = viewModel.deviceManager.get(deviceId)
        for (feature: Feature in device.featureManager.featureList) {
            val switchPreference = SwitchSettingsPreference(preferenceContext)
            switchPreference.apply {
                title = feature.displayName
                isPersistent = false
                isChecked = feature.enabled
                fragment = feature.settingsFragmentCanonicalName
                setOnPreferenceChangeListener { _, newValue ->
                    device.featureManager.setFeatureEnabled(context, feature.key, newValue as Boolean)
                    true
                }
            }

            preferenceScreen.addPreference(switchPreference)
        }
    }
}
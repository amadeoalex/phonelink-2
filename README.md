# Phone Link

Rewritten from scratch version of an app connecting Android powered device with Windows 10 powered device.

## Project status

 - [x] Alive - in development

## Installation

For now the only way to use the app is to download the sources and build it.

## Built With

* [Android Studio](https://developer.android.com/studio/)
* [Gradle](https://gradle.org/)

## Authors

Paweł Wojtczak

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.
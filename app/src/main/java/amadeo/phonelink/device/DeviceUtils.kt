package amadeo.phonelink.device

import amadeo.phonelink.utils.SecurityUtils
import amadeo.phonelink.device.connection.PACKAGE_TYPE_IDENTITY
import amadeo.phonelink.device.DeviceDiscovery.Companion.PACKAGE_TYPE_LOOKUP
import amadeo.phonelink.device.DeviceDiscovery.Companion.PACKAGE_TYPE_LOOKUP_RESPONSE
import amadeo.phonelink.service.NetPackage
import android.os.Build
import android.util.Base64

class DeviceUtils {
    companion object {
        fun getDeviceName(): String {
            return Build.MODEL
        }

        fun getDeviceType(): String {
            //TODO("differentiate between phone and tablet")
            return Device.TYPE_PHONE
        }

        fun getDeviceLookupResponsePackage(): NetPackage {
            return NetPackage(PACKAGE_TYPE_LOOKUP_RESPONSE).apply {
                put(Device.KEY_ID, getDeviceName())
                put(Device.KEY_TYPE, getDeviceType())
            }
        }

        fun getDeviceLookupPackage(): NetPackage {
            return NetPackage(PACKAGE_TYPE_LOOKUP).apply {
                put(Device.KEY_ID, getDeviceName())
                put(Device.KEY_TYPE, getDeviceType())
            }
        }

        fun getDeviceIdentityPackage(port: Int): NetPackage {
            return getDeviceLookupPackage().apply {
                type = PACKAGE_TYPE_IDENTITY
                put(Device.KEY_TCP_PORT, port)
                put(Device.KEY_CERTIFICATE, Base64.encodeToString(SecurityUtils.deviceCertificate?.encoded, Base64.NO_WRAP))
            }
        }
    }
}
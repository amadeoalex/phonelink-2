package amadeo.phonelink.vmodel

import amadeo.phonelink.device.DeviceManager
import amadeo.phonelink.device.PairingManager
import amadeo.phonelink.device.DeviceDiscovery
import amadeo.phonelink.service.ForegroundService
import amadeo.phonelink.service.UDPServer
import android.app.Application
import android.content.ComponentName
import android.os.IBinder

class PairingViewModel(application: Application) : NotifyingAndroidServiceViewModel(application, ForegroundService::class.java) {
    lateinit var udpServer: UDPServer
    lateinit var deviceManager: DeviceManager
    lateinit var pairingManager: PairingManager


    override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
        val service = (binder as ForegroundService.PhoneLinkBinder).getService()

        udpServer = service.udpServer
        deviceManager = service.deviceManager
        pairingManager = service.pairingManager

        setViewModelReady()
    }
}
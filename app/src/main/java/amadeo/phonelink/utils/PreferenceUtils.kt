package amadeo.phonelink.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import org.json.JSONObject

data class PreferenceSettingsOwner(val classType: Class<out Any>, val appendix: String = "")

data class PreferenceSetting<T>(val key: String, val default: T, var settingOwner: String = "") {
    val preferenceKey: String
        get() {
            return key + settingOwner
        }
}

class PreferenceSettings(serialized: String, val preferenceKey: String) :
    JSONObject(serialized) {

    constructor(preferenceKey: String) : this("{}", preferenceKey)

    operator fun <T : Any> get(preferenceSetting: PreferenceSetting<T>): T {
        if (!has(preferenceSetting.preferenceKey))
            put(preferenceSetting.preferenceKey, preferenceSetting.default)

        val value = get(preferenceSetting.preferenceKey)
        require(value::class.java == preferenceSetting.default::class.java) {
            "saved preference ${preferenceSetting.preferenceKey} is type ${value::class.java.simpleName} instead of requested ${preferenceSetting.default::class.java.simpleName}"
        }

        @Suppress("UNCHECKED_CAST")
        return value as T
    }

    operator fun <T : Any> set(preferenceSetting: PreferenceSetting<T>, value: T) {
        put(preferenceSetting.preferenceKey, value)
    }
}

fun Context.getPreferenceSettings(classType: Class<out Any>, appendix: String): PreferenceSettings {
    val preferences = PreferenceManager.getDefaultSharedPreferences(this)
    val preferenceKey = classType.simpleName + appendix
    val serializedPreference = preferences.getString(preferenceKey, "{}") ?: "{}"
    return PreferenceSettings(serializedPreference, preferenceKey)
}

fun Context.getPreferenceSettings(preferenceSettingsOwner: PreferenceSettingsOwner): PreferenceSettings {
    return getPreferenceSettings(preferenceSettingsOwner.classType, preferenceSettingsOwner.appendix)
}

fun Context.savePreferenceSettings(classType: Class<out Any>, appendix: String, preferenceSettings: PreferenceSettings) {
    val preferences = PreferenceManager.getDefaultSharedPreferences(this)
    val preferenceKey = classType.simpleName + appendix
    preferences.edit().apply {
        putString(preferenceKey, preferenceSettings.toString())
        apply()
    }
}

fun Context.savePreferenceSettings(preferenceSettingsOwner: PreferenceSettingsOwner, preferenceSettings: PreferenceSettings) {
    savePreferenceSettings(preferenceSettingsOwner.classType, preferenceSettingsOwner.appendix, preferenceSettings)
}


package amadeo.phonelink.ui.fragment

import amadeo.phonelink.R
import amadeo.phonelink.utils.SAFUtils
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.DocumentsContract
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.documentfile.provider.DocumentFile
import androidx.preference.PreferenceDialogFragmentCompat

private const val TAG = "PL ASDPDialogFragment"
private const val REQUEST_CODE_OPEN_DOCUMENT_TREE = 1

class DirectoryPickerDialogFragment : PreferenceDialogFragmentCompat(), View.OnClickListener {

    private lateinit var editText: EditText
    private var uri: Uri = Uri.EMPTY

    companion object {
        fun newInstance(key: String): DirectoryPickerDialogFragment {
            return DirectoryPickerDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_KEY, key)
                }
            }
        }
    }

    interface OnDirectorySelectedListener {
        fun onSelected(uri: Uri) {}
    }

    var onDirectorySelectedListener: OnDirectorySelectedListener = object : OnDirectorySelectedListener {}

    override fun onBindDialogView(view: View?) {
        super.onBindDialogView(view)

        if (view == null)
            return

        editText = view.findViewById(R.id.path)
        editText.inputType = InputType.TYPE_NULL
        editText.setOnClickListener(this)
    }

    override fun onDialogClosed(positiveResult: Boolean) {
        if (positiveResult)
            onDirectorySelectedListener.onSelected(uri)
    }

    override fun onClick(v: View?) {
        startActivityForResult(Intent(Intent.ACTION_OPEN_DOCUMENT_TREE), REQUEST_CODE_OPEN_DOCUMENT_TREE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK || data == null)
            return

        when (requestCode) {
            REQUEST_CODE_OPEN_DOCUMENT_TREE -> {
                Log.d(TAG, "result ok")

                uri = data.data ?: return
                val path = DocumentsContract.getTreeDocumentId(uri)
                editText.setText(path)
            }
        }
    }


}

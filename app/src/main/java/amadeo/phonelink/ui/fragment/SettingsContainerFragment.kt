package amadeo.phonelink.ui.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import amadeo.phonelink.R
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.Navigation
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat

private const val TAG = "PL SettingsCF"
private const val ARG_SETTINGS_FRAGMENT = "fragment"
private const val ARG_DEVICE_ID = "deviceId"
private const val ARG_CHILD_BUNDLE = "childBundle"

class SettingsContainerFragment : Fragment(), PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

    companion object {
        fun createBundle(fragmentClass: Class<*>, childBundle: Bundle = Bundle.EMPTY): Bundle {
            require(fragmentClass.canonicalName != null) { throw IllegalArgumentException("class canonical name cannot be null") }
            return createBundle(fragmentClass.canonicalName!!, childBundle)
        }

        fun createBundle(canonicalClassName: String, childBundle: Bundle = Bundle.EMPTY): Bundle {
            return Bundle().apply {
                putString(ARG_SETTINGS_FRAGMENT, canonicalClassName)
                putBundle(ARG_CHILD_BUNDLE, childBundle)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_settings_container, container, false)

        require(arguments != null && arguments?.getString(ARG_SETTINGS_FRAGMENT) != null) { throw IllegalArgumentException("missing fragment class argument") }
        //require(arguments != null && arguments?.getString(ARG_CHILD_BUNDLE) != null) { throw IllegalArgumentException("missing child bundle") }

        val fragmentManager: FragmentManager = childFragmentManager

        val className: String = arguments!!.getString(ARG_SETTINGS_FRAGMENT)!!
        val fragment = fragmentManager.fragmentFactory.instantiate(activity!!.classLoader, className).apply {
            arguments = this@SettingsContainerFragment.arguments?.getBundle(ARG_CHILD_BUNDLE)
        }

        require(fragment is SettingsFragment) { throw IllegalArgumentException("provided fragment class is not instance of ${SettingsFragment::class.java}") }

        fragmentManager.beginTransaction()
            .replace(R.id.settings_container, fragment)
            .commit()

        return view
    }

    override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat, preference: Preference): Boolean {
        //val actionString = preference.fragment.substring(preference.fragment.lastIndexOf(".") + 1).decapitalize()
        //val actionId = getResourceIdByName("action_$actionString", R.id::class.java)
        //require(actionId != -1) { throw IllegalArgumentException("there is no action $actionString associated with provided fragment") }

        val actionId = R.id.action_settingsContainerFragment
        val bundle = createBundle(preference.fragment, caller.arguments ?: Bundle.EMPTY)

        require(view != null) { throw IllegalStateException("view should not be null at this point") }
        Navigation.findNavController(view!!).navigate(actionId, bundle)
        return true
    }


    private fun getResourceIdByName(resourceName: String, c: Class<*>): Int {
        return try {
            val field = c.getDeclaredField(resourceName)
            field.getInt(field)
        } catch (e: Exception) {
            Log.i(TAG, "cannot locate resource with name: $resourceName")
            -1
        }
    }
}

abstract class SettingsFragment : PreferenceFragmentCompat() {

    var title: String
        get() {
            return (activity as AppCompatActivity).supportActionBar?.title as String
        }
        set(value) {
            (activity as AppCompatActivity).supportActionBar?.title = value
        }

    override fun getCallbackFragment(): SettingsContainerFragment? {
        Log.d("PL TEST", "parent fragment was ${parentFragment!!::class.java}")
        return findSettingsContainer(parentFragment)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (preferenceScreen != null && preferenceScreen.title != null)
            title = preferenceScreen.title as String
    }

    private fun findSettingsContainer(fragment: Fragment?): SettingsContainerFragment? {
        if (fragment == null)
            return fragment

        return if (fragment is SettingsContainerFragment)
            fragment
        else {
            findSettingsContainer(fragment.parentFragment) ?: fragment as SettingsContainerFragment
        }
    }
}
package amadeo.phonelink.vmodel

import amadeo.phonelink.device.DeviceManager
import amadeo.phonelink.service.ForegroundService
import android.app.Application
import android.content.ComponentName
import android.os.IBinder

class DevicesViewModel(application: Application) : NotifyingAndroidServiceViewModel(application, ForegroundService::class.java) {
    lateinit var deviceManager: DeviceManager

    override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
        val service = (binder as ForegroundService.PhoneLinkBinder).getService()
        deviceManager = service.deviceManager

        setViewModelReady()
    }
}
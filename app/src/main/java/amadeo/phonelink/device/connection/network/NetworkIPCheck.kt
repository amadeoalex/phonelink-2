package amadeo.phonelink.device.connection.network

import amadeo.phonelink.device.Device
import amadeo.phonelink.device.Device.Companion.KEY_ID
import amadeo.phonelink.service.IUDPNetPackageHandler
import amadeo.phonelink.service.NetPackage
import amadeo.phonelink.service.UDPServer
import amadeo.phonelink.utils.SecurityUtils
import android.content.Context
import android.util.Base64
import android.util.Log
import java.lang.ref.WeakReference
import java.net.InetSocketAddress

/**
 * Package should contain:
 * [KEY_ID] - id of the device requesting ip check
 * [KEY_IP_CHECK_DATA] - base64 string of bytes to be decrypted and sent back
 */
private const val PACKAGE_TYPE_IP_CHECK = "ipCheck"

/**
 * Package should contain:
 * [KEY_ID] - id of the device which requested the ip check
 * [KEY_IP_CHECK_DATA] - decrypted data in string form
 */
private const val PACKAGE_TYPE_IP_CHECK_RESPONSE = "ipCheckResponse"

private const val KEY_IP_CHECK_DATA = "ipCheckToken"

private const val TOKEN_LIFE_SPAN = 1000 * 5

private const val TAG = "PL NetworkIPCheck"

class NetworkIPCheck(context: Context, private val device: Device, private val udpServer: UDPServer) : IUDPNetPackageHandler {

    private data class CheckToken(val token: String, val expirationTime: Long)

    private val contextWeakReference = WeakReference(context)
    private val tokens: MutableList<CheckToken> = mutableListOf()

    override val packageTypes: List<String> = listOf(PACKAGE_TYPE_IP_CHECK, PACKAGE_TYPE_IP_CHECK_RESPONSE)

    var ipCheckRequired = false
        private set

    init {
        udpServer.addPackageHandler(this)
    }

    fun perform() {
        if (!ipCheckRequired)
            return

        //TODO("consider making device address variable volatile")
        Log.d(TAG, "check ip for ${device.id}")

        sendIPCheckRequest()

        Thread.sleep(2000)
    }

    fun onNetPackageFailed(): Boolean {
        return if (ipCheckRequired) {
            ipCheckRequired = false
            true
        } else
            false
    }

    private fun sendIPCheckRequest() {
        val ipCheckToken = SecurityUtils.generateRandomString()
        tokens.add(CheckToken(ipCheckToken, System.currentTimeMillis() + TOKEN_LIFE_SPAN))

        val encryptedData = SecurityUtils.encrypt(contextWeakReference.get()!!, ipCheckToken.toByteArray())
        udpServer.sendNetPackage(NetPackage(PACKAGE_TYPE_IP_CHECK).apply {
            put(KEY_ID, device.id)
            put(KEY_IP_CHECK_DATA, Base64.encode(encryptedData, 0))
        }, InetSocketAddress(device.address, UDPServer.LISTEN_PORT))
    }

    override fun handleUDPNetPackage(netPackage: NetPackage, inetSocketAddress: InetSocketAddress): Boolean {
        if (!netPackage.has(KEY_ID) || netPackage.getString(KEY_ID) != device.id)
            return false

        return when (netPackage.type) {
            PACKAGE_TYPE_IP_CHECK -> {
                require(netPackage.has(KEY_IP_CHECK_DATA)) { "missing ${::KEY_IP_CHECK_DATA.name}" }

                tokens.removeAll { token -> token.expirationTime < System.currentTimeMillis() }

                val encryptedData = Base64.decode(netPackage.getString(KEY_IP_CHECK_DATA), 0)
                val data = String(SecurityUtils.decrypt(contextWeakReference.get()!!, encryptedData))

                udpServer.sendNetPackage(NetPackage(PACKAGE_TYPE_IP_CHECK_RESPONSE).apply {
                    put(KEY_ID, device.id)
                    put(KEY_IP_CHECK_DATA, data)
                }, inetSocketAddress)

                true
            }

            PACKAGE_TYPE_IP_CHECK_RESPONSE -> {
                require(netPackage.has(KEY_IP_CHECK_DATA)) { "missing ${::KEY_IP_CHECK_DATA.name}" }

                tokens.removeAll { token -> token.expirationTime < System.currentTimeMillis() }

                if (tokens.firstOrNull { token -> token.token == netPackage.getString(KEY_IP_CHECK_DATA) } != null) {
                    Log.d(TAG, "updating ${device.id} address from ${device.address} to ${inetSocketAddress.address.hostAddress}")
                    device.address = inetSocketAddress.address.hostAddress
                } else
                    Log.w(TAG, "received ip check response but token is not valid")

                true
            }

            else -> false
        }
    }

}
package amadeo.phonelink.service

import org.json.JSONObject

private const val KEY_PACKAGE_TYPE = "__type__"

const val PACKAGE_TYPE_DEVICE_DISCONNECT = "__DISCONNECT__"

class NetPackage(type: String = "", serialized: String = "{}") : JSONObject(serialized) {

    var type: String
        get() {
            return if (this.has(KEY_PACKAGE_TYPE))
                this.getString(KEY_PACKAGE_TYPE)
            else
                PACKAGE_TYPE_DEVICE_DISCONNECT
        }
        set(value) {
            this.put(KEY_PACKAGE_TYPE, value)
        }

        init {
            if(type.isNotEmpty()){
                this.type = type
            }
        }
}
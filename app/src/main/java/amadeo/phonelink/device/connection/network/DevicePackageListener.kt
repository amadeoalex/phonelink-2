package amadeo.phonelink.device.connection.network

import amadeo.phonelink.service.NetPackage
import android.util.Log
import java.io.BufferedReader
import java.lang.Exception

private const val TAG = "PL DPackage Listener"

class DevicePackageListener(private val bufferedReader: BufferedReader) : Thread() {

    init {
        name = TAG
    }

    interface OnNetPackageEventListener {
        fun onNetPackageReceived(netPackage: NetPackage)
        fun onConnectionBroken()
    }

    //TODO("unify usage of default listeners - interface with default body or this")
    var netPackageEventListener = object : OnNetPackageEventListener {
        override fun onConnectionBroken() {}
        override fun onNetPackageReceived(netPackage: NetPackage) {}
    }

    private var running = true

    override fun run() {
        Log.d(TAG, "started")

        while (running) {
            try {
                val netPackage = NetPackage(serialized = bufferedReader.readLine())
                Log.d(TAG, "received package ${netPackage.type} ($netPackage)")

                netPackageEventListener.onNetPackageReceived(netPackage)
            } catch (e: Exception) {
                Log.d(TAG, "connection problem, breaking: ${e.message}")
                Thread.currentThread().interrupt()

                netPackageEventListener.onConnectionBroken()
            }
        }

        Log.d(TAG, "thread finished")
    }


    override fun interrupt() {
        super.interrupt()

        if(running){
            running = false
            Log.d(TAG, "interrupted")
        }
    }
}
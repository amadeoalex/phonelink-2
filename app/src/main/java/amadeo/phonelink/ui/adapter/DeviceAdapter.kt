package amadeo.phonelink.ui.adapter

import amadeo.phonelink.R
import amadeo.phonelink.device.Device
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

private const val TAG = "PL DeviceAdapter"

class DeviceAdapter : ListAdapter<Device, DeviceAdapter.DeviceHolder>(DeviceDiffUtil()) {

    interface OnItemClickedListener {
        fun onItemClicked(device: Device, holder: DeviceHolder)
    }

    interface OnItemOptionsMenuClickedListener {
        fun onOptionsMenuClicked(view: View, device: Device)
    }

    var itemClickedListener: OnItemClickedListener = object : OnItemClickedListener {
        override fun onItemClicked(device: Device, holder: DeviceHolder) {}
    }

    var itemMenuClickedListener: OnItemOptionsMenuClickedListener = object : OnItemOptionsMenuClickedListener {
        override fun onOptionsMenuClicked(view: View, device: Device) {}
    }


    inner class DeviceHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val deviceId: TextView = itemView.findViewById(R.id.device_id)
        val options: ImageButton = itemView.findViewById(R.id.options)
    }

    class DeviceDiffUtil : DiffUtil.ItemCallback<Device>() {
        override fun areItemsTheSame(oldItem: Device, newItem: Device): Boolean {
            return oldItem.id == newItem.id && oldItem.type == newItem.type
        }

        override fun areContentsTheSame(oldItem: Device, newItem: Device): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_device, parent, false)

        return DeviceHolder(itemView)
    }

    override fun onBindViewHolder(holder: DeviceHolder, position: Int) {
        val device = getItem(holder.adapterPosition)

        holder.itemView.setOnClickListener {
            itemClickedListener.onItemClicked(getItem(holder.adapterPosition), holder)
        }

        holder.options.setOnClickListener {
            itemMenuClickedListener.onOptionsMenuClicked(holder.options, getItem(holder.adapterPosition))
        }

        holder.deviceId.text = device.id
    }
}
package amadeo.phonelink.ui.fragment


import amadeo.phonelink.R
import amadeo.phonelink.device.Device
import amadeo.phonelink.ui.adapter.DeviceAdapter
import amadeo.phonelink.utils.runOnBackgroundThread
import amadeo.phonelink.vmodel.DevicesViewModel
import amadeo.phonelink.vmodel.NotifyingAndroidServiceViewModel
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar

class DevicesFragment : Fragment(), DeviceAdapter.OnItemClickedListener, DeviceAdapter.OnItemOptionsMenuClickedListener {
    private lateinit var viewModel: DevicesViewModel

    private lateinit var deviceAdapter: DeviceAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_devices, container, false)

        deviceAdapter = DeviceAdapter().apply {
            itemClickedListener = this@DevicesFragment
            itemMenuClickedListener = this@DevicesFragment
        }


        viewManager = LinearLayoutManager(activity)

        recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view).apply {
            layoutManager = viewManager
            adapter = deviceAdapter
        }

        viewModel = ViewModelProvider(activity!!)[DevicesViewModel::class.java]

        viewModel.setOnReadyListener(object : NotifyingAndroidServiceViewModel.OnViewModelReadyListener {
            override fun onViewModelReady() {
                setupObserved()
            }
        })

        return view
    }

    private fun setupObserved() {
        viewModel.deviceManager.devices.observe(this@DevicesFragment, Observer<List<Device>> { devices ->
            Log.d("PL DevicesFragment", "changed")
            deviceAdapter.submitList(ArrayList(devices))
        })
    }

    override fun onItemClicked(device: Device, holder: DeviceAdapter.DeviceHolder) {

    }

    //TODO("create expandable card views instead of option menu")
    override fun onOptionsMenuClicked(view: View, device: Device) {
        PopupMenu(view.context, view).apply {
            menuInflater.inflate(R.menu.fragment_devices_item_options_menu, menu)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.details -> {
                        Snackbar.make(view, "Create expandable card views...", Snackbar.LENGTH_LONG).show()
                        true
                    }

                    R.id.settings -> {
                        val childBundle = DeviceSettingsFragment.createBundle(device.id)
                        Navigation.findNavController(view)
                            .navigate(
                                R.id.action_settingsContainerFragment,
                                SettingsContainerFragment.createBundle(DeviceSettingsFragment::class.java, childBundle)
                            )
                        true
                    }

                    R.id.disconnect -> {
                        runOnBackgroundThread({ device.connection.disconnect() }, true)
                        true
                    }

                    R.id.unpair -> {
                        runOnBackgroundThread({ device.connection.interrupt() }, true)
                        viewModel.deviceManager.remove(device)
                        Snackbar.make(view, "${device.id} removed", Snackbar.LENGTH_SHORT)
                            .setAction(R.string.undo) {
                                viewModel.deviceManager.add(device)
                            }
                            .show()

                        true
                    }

                    else -> false
                }
            }
            show()
        }
    }
}

package amadeo.phonelink.service

import amadeo.phonelink.device.DeviceDiscovery
import amadeo.phonelink.device.DeviceManager
import amadeo.phonelink.device.PairingManager

interface PhoneLinkService {
    var udpServer: UDPServer
    var deviceManager: DeviceManager
    var pairingManager: PairingManager
}
package amadeo.phonelink.device

import amadeo.phonelink.device.connection.PairingProcess
import amadeo.phonelink.service.UDPServer
import android.content.Context
import android.os.AsyncTask
import java.lang.ref.WeakReference

class PairingManager(context: Context, val deviceManager: DeviceManager, private val udpServer: UDPServer) {

    private val contextWeakReference: WeakReference<Context> = WeakReference(context)
    private val pairingProcesses: MutableMap<Device, PairingProcess> = mutableMapOf()

    @Synchronized
    fun getPairingProcess(device: Device, isClient: Boolean): PairingProcess {
        if (!pairingProcesses.containsKey(device) || pairingProcesses[device] == null || pairingProcesses[device]?.status == AsyncTask.Status.FINISHED)
            pairingProcesses[device] = PairingProcess(contextWeakReference.get()!!, deviceManager, device, isClient, udpServer)

        return pairingProcesses[device]!!
    }

    @Synchronized
    fun cleanup() {
        for ((_, pairingProcess) in pairingProcesses) {
            pairingProcess.cancel(true)
        }
    }

}
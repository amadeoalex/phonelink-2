package amadeo.phonelink.device.connection

import amadeo.phonelink.service.NetPackage

abstract class DeviceConnection : Thread() {
    companion object {
        val NoConnection = object : DeviceConnection() {}
    }

    open fun sendNetPackage(netPackage: NetPackage) {}
    open fun disconnect() {}
}
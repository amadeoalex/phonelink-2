package amadeo.phonelink.feature.dummy

import amadeo.phonelink.R
import amadeo.phonelink.ui.fragment.SettingsFragment
import android.os.Bundle

class DummyFeatureSettingsFragment : SettingsFragment() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences_feature_dummy, rootKey)
    }

}
package amadeo.phonelink.feature

import amadeo.phonelink.device.Device
import amadeo.phonelink.service.NetPackage
import android.content.Context
import androidx.annotation.CallSuper

abstract class Feature {
    abstract val TAG: String

    abstract val key: String
    abstract val netPackagePrefix: String

    abstract val displayName: String
    var settingsFragmentCanonicalName: String? = null

    abstract val device: Device

    var enabled: Boolean = false

    @CallSuper
    open fun onEnabled(context: Context) {
        if (enabled)
            return

        enabled = true
    }

    @CallSuper
    open fun onDisabled(context: Context) {
        if (!enabled)
            return

        enabled = false
    }

    abstract fun onConnected()
    abstract fun onNetPackageReceived(netPackage: NetPackage)
    abstract fun onNetPackageFailed(netPackage: NetPackage)
    abstract fun onDisconnected()

    protected fun getTagExtension(): String {
        return "of ${device.id}: "
    }
}
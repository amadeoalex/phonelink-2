package amadeo.phonelink.utils

import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception

class JSONUtils {
    companion object {
        val InvalidJSONObject = JSONObject()
        val InvalidJSONArray = JSONArray()

        fun verifyJSONObject(serialized: String): JSONObject {
            var jsonObject: JSONObject = InvalidJSONObject

            try {
                jsonObject = JSONObject(serialized)
            } catch (e: Exception) {
            }

            return jsonObject
        }

        fun verifyJSONArray(serialized: String): JSONArray {
            var jsonArray: JSONArray = InvalidJSONArray

            try {
                jsonArray = JSONArray(serialized)
            } catch (e: Exception) {
            }

            return jsonArray
        }

    }
}
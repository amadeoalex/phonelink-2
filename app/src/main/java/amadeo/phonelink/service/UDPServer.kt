package amadeo.phonelink.service

import amadeo.phonelink.BuildConfig
import amadeo.phonelink.device.Device
import amadeo.phonelink.device.DeviceUtils
import amadeo.phonelink.device.connection.DeviceConnection
import amadeo.phonelink.device.types.Phone
import amadeo.phonelink.utils.NetUtils
import android.util.Log
import androidx.lifecycle.MutableLiveData
import java.lang.ref.WeakReference
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.ClosedByInterruptException
import java.nio.channels.DatagramChannel
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

private const val TAG = "PL UDPServer"

interface IUDPNetPackageHandler {
    val packageTypes: List<String>
    fun handleUDPNetPackage(netPackage: NetPackage, inetSocketAddress: InetSocketAddress): Boolean
}

class UDPServer : Thread() {
    companion object {
        const val LISTEN_PORT = 4848
    }

    private val packageHandlers: MutableList<WeakReference<IUDPNetPackageHandler>> = mutableListOf()
    private val datagramChannel: DatagramChannel = DatagramChannel.open()

    init {
        name = TAG

        datagramChannel.socket().bind(InetSocketAddress(LISTEN_PORT))
        datagramChannel.socket().broadcast = true
    }

    var running: Boolean = true
        private set

    override fun run() {
        try {
            val buffer: ByteBuffer = ByteBuffer.allocate(1024 * 1024)
            while (running) {
                buffer.clear()
                val socketAddress = datagramChannel.receive(buffer)
                buffer.flip()

                val netPackage = NetPackage(serialized = StandardCharsets.UTF_8.decode(buffer).toString())
                netPackageReceived(netPackage, socketAddress as InetSocketAddress)
            }
            //TODO("add catch for json exception")
        } catch (e: ClosedByInterruptException) {
            Log.d(TAG, "closed by exception")
        }

    }

    private fun netPackageReceived(netPackage: NetPackage, inetSocketAddress: InetSocketAddress) {
        //TODO("use device id instead of device model")
        if (netPackage.optString(Device.KEY_ID) != DeviceUtils.getDeviceName()) {
            packageHandlers.removeAll { it.get() == null }

            var handled = false
            for (handlerReference in packageHandlers) {
                val handler = handlerReference.get() as IUDPNetPackageHandler
                if (netPackage.type in handler.packageTypes)
                    if (handler.handleUDPNetPackage(netPackage, inetSocketAddress))
                        handled = true
            }

            if (!handled)
                Log.d(TAG, "unknown package received '${netPackage.type}'")
        }
    }

    @Synchronized
    fun addPackageHandler(handler: IUDPNetPackageHandler) {
        packageHandlers.add(WeakReference(handler))
        packageHandlers.removeAll { it.get() == null }
    }

    @Synchronized
    fun sendNetPackage(netPackage: NetPackage, socketAddress: InetSocketAddress) {
        val buffer = ByteBuffer.wrap(netPackage.toString().toByteArray())
        datagramChannel.send(buffer, socketAddress)
    }

    override fun interrupt() {
        super.interrupt()

        if (running) {
            running = false

            datagramChannel.close()
        }
    }
}
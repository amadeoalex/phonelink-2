package amadeo.phonelink.utils

import amadeo.phonelink.device.Device
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import androidx.preference.PreferenceManager
import android.util.Base64
import android.util.Log
import org.bouncycastle.asn1.x500.X500NameBuilder
import org.bouncycastle.asn1.x500.style.BCStyle
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import java.lang.Exception
import java.math.BigInteger
import java.security.*
import java.security.cert.X509Certificate
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*
import javax.crypto.Cipher
import javax.net.ssl.*

private const val TAG = "PL SecurityUtils"

private const val KEY_PUBLIC_KEY = "publicKey"
private const val KEY_PRIVATE_KEY = "privateKey"
private const val KEY_CERTIFICATE = "deviceCertificate"

private fun <T> Collection<T>.random(random: Random): T {
    if (isEmpty())
        throw NoSuchElementException("Collection is empty.")
    return elementAt(random.nextInt(size))
}

class SecurityUtils {
    companion object {
        private val bouncyCastleProvider = BouncyCastleProvider()

        private val secureRandom = SecureRandom()
        private val characterPool = ('a'..'z') + ('0'..'9') + ('A'..'Z')

        var deviceCertificate: X509Certificate? = null
            private set

        fun init(context: Context) {
            if (deviceCertificate != null)
                return

            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

            try {

                if (!sharedPreferences.contains(KEY_PUBLIC_KEY) && !sharedPreferences.contains(KEY_PRIVATE_KEY))
                    generateDeviceKeyPair(sharedPreferences)

                if (!sharedPreferences.contains(KEY_CERTIFICATE))
                    generateDeviceCertificate(context, sharedPreferences)

                val deviceEncodedCertificate = Base64.decode(sharedPreferences.getString(KEY_CERTIFICATE, ""), 0)
                val certificateHolder = X509CertificateHolder(deviceEncodedCertificate)
                deviceCertificate = JcaX509CertificateConverter().setProvider(bouncyCastleProvider).getCertificate(certificateHolder)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private fun generateDeviceKeyPair(sharedPreferences: SharedPreferences) {
            Log.d(TAG, "generating new device key pair")

            val keyPairGenerator = KeyPairGenerator.getInstance("RSA").apply {
                initialize(2048)
            }
            val keyPair = keyPairGenerator.genKeyPair()

            val encodedPublicKey = Base64.encodeToString(keyPair.public.encoded, 0)
            val encodedPrivateKey = Base64.encodeToString(keyPair.private.encoded, 0)

            sharedPreferences.edit().apply {
                putString(KEY_PUBLIC_KEY, encodedPublicKey)
                putString(KEY_PRIVATE_KEY, encodedPrivateKey)
                apply()
            }
        }

        fun getPublicKey(context: Context): PublicKey {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val publicKeyBytes: ByteArray = Base64.decode(sharedPreferences.getString(KEY_PUBLIC_KEY, ""), 0)
            return KeyFactory.getInstance("RSA").generatePublic(X509EncodedKeySpec(publicKeyBytes))
        }

        fun getPrivateKey(context: Context): PrivateKey {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val privateKeyBytes: ByteArray = Base64.decode(sharedPreferences.getString(KEY_PRIVATE_KEY, ""), 0)
            return KeyFactory.getInstance("RSA").generatePrivate(PKCS8EncodedKeySpec(privateKeyBytes))
        }

        private fun generateDeviceCertificate(context: Context, sharedPreferences: SharedPreferences): X509Certificate {
            Log.d(TAG, "generating new device certificate")

            val nameBuilder = X500NameBuilder(BCStyle.INSTANCE).apply {
                addRDN(BCStyle.CN, Build.MODEL)
                addRDN(BCStyle.OU, "Phone Link")
                addRDN(BCStyle.O, "Amadeo")
            }

            val calendar = Calendar.getInstance()
            val from = calendar.time
            calendar.add(Calendar.YEAR, 5)
            val to = calendar.time

            val certificateBuilder = JcaX509v3CertificateBuilder(
                nameBuilder.build(),
                BigInteger.ONE,
                from,
                to,
                nameBuilder.build(),
                getPublicKey(context)
            )
            val contentSigner = JcaContentSignerBuilder("SHA256withRSA")
                .setProvider(bouncyCastleProvider)
                .build(getPrivateKey(context))

            val certificate = JcaX509CertificateConverter().setProvider(bouncyCastleProvider)
                .getCertificate(certificateBuilder.build(contentSigner))

            sharedPreferences.edit().apply {
                putString(KEY_CERTIFICATE, Base64.encodeToString(certificate.encoded, 0))
                apply()
            }

            return certificate
        }

        fun getSSLContext(context: Context, device: Device = Device.InvalidDevice): SSLContext {
            val keyStorePassword: CharArray = charArrayOf(' ')

            val keyStore = KeyStore.getInstance(KeyStore.getDefaultType()).apply {
                load(null, null)
                setKeyEntry(
                    KEY_PRIVATE_KEY,
                    getPrivateKey(context), keyStorePassword, arrayOf(deviceCertificate)
                )
            }

            val trustManager = if (device != Device.InvalidDevice) {
                val certificateHolder = X509CertificateHolder(device.certificate)
                val remoteDeviceCertificate =
                    JcaX509CertificateConverter().setProvider(bouncyCastleProvider).getCertificate(certificateHolder)
                keyStore.setCertificateEntry(device.id, remoteDeviceCertificate)

                val trustManagerFactory =
                    TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).apply {
                        init(keyStore)
                    }

                trustManagerFactory.trustManagers
            } else {
                Log.d(TAG, "generating insecure SSL context!")

                arrayOf(object : X509TrustManager {
                    override fun checkClientTrusted(x509Certificates: Array<X509Certificate>, s: String) {

                    }

                    override fun checkServerTrusted(x509Certificates: Array<X509Certificate>, s: String) {

                    }

                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return emptyArray()
                    }
                })
            }

            val keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm()).apply {
                init(keyStore, keyStorePassword)
            }

            return SSLContext.getInstance("TLS").apply {
                init(keyManagerFactory.keyManagers, trustManager, SecureRandom())
            }
        }

        fun generateRandomString(min: Int = 32, max: Int = 128): String {
            val length = secureRandom.nextInt((max - min) + 1) + min
            return (0..length)
                .map { characterPool.random(secureRandom) }
                .joinToString("")
        }

        fun encrypt(context: Context, data: ByteArray, publicKey: PublicKey = getPublicKey(context)): ByteArray {
            //TODO("think again about using PKCS1Padding")
            val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
            cipher.init(Cipher.ENCRYPT_MODE, publicKey)
            return cipher.doFinal(data)
        }

        fun decrypt(context: Context, data: ByteArray, privateKey: PrivateKey = getPrivateKey(context)): ByteArray {
            val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
            cipher.init(Cipher.DECRYPT_MODE, privateKey)
            return cipher.doFinal(data)
        }
    }
}
package amadeo.phonelink.feature

import amadeo.phonelink.device.Device
import amadeo.phonelink.service.NetPackage
import android.content.Context
import android.util.Log

class EmptyFeature3(override val device: Device) : Feature() {
    override val TAG = "PL EmptyFeature3"
    override val key = "feature_empty3"
    override val netPackagePrefix = "ef3_"

    override val displayName = "Empty Feature3"

    override fun onEnabled(context: Context) {
        super.onEnabled(context)
        Log.d(TAG, getTagExtension() + "started")
    }

    override fun onDisabled(context: Context) {
        super.onDisabled(context)
        Log.d(TAG, getTagExtension() + "stopped")
    }

    override fun onConnected() {

    }

    override fun onNetPackageReceived(netPackage: NetPackage) {

    }

    override fun onNetPackageFailed(netPackage: NetPackage) {

    }

    override fun onDisconnected() {

    }

}
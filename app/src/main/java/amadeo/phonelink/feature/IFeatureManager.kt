package amadeo.phonelink.feature

import amadeo.phonelink.service.NetPackage
import android.content.Context

interface IFeatureManager {
    companion object NoFeatureManager : IFeatureManager {
        override val featureList: List<Feature> = listOf()
        override val enabledFeatureList: List<Feature> = listOf()
    }

    val featureList: List<Feature>
    val enabledFeatureList: List<Feature>

    fun initialize(context: Context) {}

    fun startEnabledFeatures(context: Context) {}

    fun setFeatureEnabled(context: Context, key: String, enabled: Boolean) {}

    fun getFeature(key: String): Feature? {
        return null
    }

    fun onConnected() {}

    fun onNetPackageReceived(netPackage: NetPackage) {}

    fun onNetPackageFailed(netPackage: NetPackage) {}

    fun onDisconnected() {}

    fun cleanup(context: Context) {}
}
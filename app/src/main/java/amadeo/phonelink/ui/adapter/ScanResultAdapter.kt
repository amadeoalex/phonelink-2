package amadeo.phonelink.ui.adapter

import amadeo.phonelink.R
import amadeo.phonelink.device.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class ScanResultAdapter : ListAdapter<Device, ScanResultAdapter.ScanResultHolder>(DeviceDiffUtil()) {

    interface OnItemClickedListener {
        fun onItemClicked(device: Device, holder: ScanResultHolder)
    }

    var itemClickedListener: OnItemClickedListener = object : OnItemClickedListener {
        override fun onItemClicked(device: Device, holder: ScanResultHolder) {}
    }

    inner class ScanResultHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var isClient = false
        val deviceIcon: ImageView = itemView.findViewById(R.id.device_icon)
        val deviceId: TextView = itemView.findViewById(R.id.device_id)
        val pairingStatus: TextView = itemView.findViewById(R.id.pairing_status)
        val progressBar: ProgressBar = itemView.findViewById(R.id.progress)
    }

    class DeviceDiffUtil : DiffUtil.ItemCallback<Device>() {
        override fun areItemsTheSame(oldItem: Device, newItem: Device): Boolean {
            //TODO("tweak")
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Device, newItem: Device): Boolean {
            return areItemsTheSame(oldItem, newItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScanResultHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_scan_result, parent, false)

        return ScanResultHolder(itemView)
    }

    override fun onBindViewHolder(holder: ScanResultHolder, position: Int) {
        val device = getItem(position)
        holder.deviceIcon.setImageResource(getDeviceIconResource(device))
        holder.deviceId.text = device.id

        holder.isClient = false
        holder.itemView.setOnClickListener {
            itemClickedListener.onItemClicked(getItem(holder.adapterPosition), holder)
        }
    }

    private fun getDeviceIconResource(device: Device): Int {
        return when (device.type) {
            Device.TYPE_PHONE -> R.drawable.ic_phone
            Device.TYPE_LAPTOP -> R.drawable.ic_laptop
            Device.TYPE_TABLET -> R.drawable.ic_tablet
            Device.TYPE_PC -> R.drawable.ic_computer
            else -> R.drawable.ic_device_other
        }
    }

    fun add(position: Int, device: Device) {
        val list = ArrayList<Device>(currentList)
        list.add(position, device)
        submitList(list)
    }
}
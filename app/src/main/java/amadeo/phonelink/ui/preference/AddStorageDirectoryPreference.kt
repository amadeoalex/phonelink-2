package amadeo.phonelink.ui.preference

import amadeo.phonelink.R
import android.content.Context
import android.util.AttributeSet
import androidx.preference.DialogPreference

class AddStorageDirectoryPreference(context: Context?, attributeSet: AttributeSet?) : DialogPreference(context, attributeSet) {

    init {
        dialogLayoutResource = R.layout.fragment_dialog_directory_picker
    }

}
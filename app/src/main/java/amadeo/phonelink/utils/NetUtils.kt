package amadeo.phonelink.utils

import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*

class NetUtils {
    companion object{
        fun getBroadcastAddresses() : List<InetAddress>{
            val addresses: ArrayList<InetAddress> = arrayListOf()
            val interfaces: Enumeration<NetworkInterface> = NetworkInterface.getNetworkInterfaces()
            while(interfaces.hasMoreElements()){
                val netInterface = interfaces.nextElement()
                if(netInterface.isLoopback)
                    continue

                for(interfaceAddress in netInterface.interfaceAddresses){
                    if(interfaceAddress.broadcast != null)
                        addresses.add(interfaceAddress.broadcast)
                }
            }

            return addresses
        }
    }
}
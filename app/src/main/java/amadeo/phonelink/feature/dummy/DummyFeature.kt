package amadeo.phonelink.feature.dummy

import amadeo.phonelink.device.Device
import amadeo.phonelink.feature.Feature
import amadeo.phonelink.service.NetPackage
import android.content.Context
import android.util.Log

class DummyFeature(override val device: Device) : Feature() {
    companion object {
        //TODO("use R.string resource identifiers instead of string literals")
        const val NET_PACKAGE_PREFIX = "df_"

        const val PACKAGE_TYPE_PING = NET_PACKAGE_PREFIX + "ping"
        const val PACKAGE_TYPE_PONG = NET_PACKAGE_PREFIX + "pong"
    }

    override val TAG = "PL DummyFeature"
    override val key = "feature_dummy"
    override val netPackagePrefix = NET_PACKAGE_PREFIX
    override val displayName = "Dummy Feature"

    init {
        settingsFragmentCanonicalName = DummyFeatureSettingsFragment::class.java.canonicalName
    }

    override fun onEnabled(context: Context) {
        super.onEnabled(context)
        Log.d(TAG, getTagExtension() + "started")
    }

    override fun onDisabled(context: Context) {
        super.onDisabled(context)
        Log.d(TAG, getTagExtension() + "stopped")
    }

    override fun onConnected() {
        Log.d(TAG, getTagExtension() + "connected")
    }

    override fun onNetPackageReceived(netPackage: NetPackage) {
        Log.d(TAG, getTagExtension() + "received net package ${netPackage.type}")

        when (netPackage.type) {
            PACKAGE_TYPE_PING -> {
                device.connection.sendNetPackage(NetPackage(PACKAGE_TYPE_PONG))
            }
        }
    }

    override fun onNetPackageFailed(netPackage: NetPackage) {
        Log.d(TAG, getTagExtension() + "net package ${netPackage.type} failed")
    }

    override fun onDisconnected() {
        Log.d(TAG, getTagExtension() + "disconnected")
    }

    fun sendPing() {
        device.connection.sendNetPackage(NetPackage(PACKAGE_TYPE_PING))
    }
}
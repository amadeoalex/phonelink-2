package amadeo.phonelink.vmodel

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import androidx.lifecycle.AndroidViewModel

abstract class NotifyingAndroidServiceViewModel(application: Application, serviceClass: Class<*>) : AndroidViewModel(application) {

    interface OnViewModelReadyListener {
        fun onViewModelReady() {}
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            this@NotifyingAndroidServiceViewModel.onServiceConnected(name, binder)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            viewModelReady = false
        }
    }

    private var viewModelReadyListener: OnViewModelReadyListener = object : OnViewModelReadyListener {}

    fun setOnReadyListener(listener: OnViewModelReadyListener) {
        viewModelReadyListener = listener
        if(viewModelReady)
            viewModelReadyListener.onViewModelReady()
    }

    var viewModelReady: Boolean = false
        private set

    init {
        val intent = Intent(application, serviceClass)
        application.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    protected fun setViewModelReady() {
        viewModelReady = true
        viewModelReadyListener.onViewModelReady()
    }

    protected abstract fun onServiceConnected(name: ComponentName?, binder: IBinder?)

    override fun onCleared() {
        getApplication<Application>().unbindService(serviceConnection)
        viewModelReady = false
    }
}
package amadeo.phonelink.ui.fragment

import android.os.Bundle

import amadeo.phonelink.R

class AppSettingsFragment : SettingsFragment() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_app_settings)
    }

}

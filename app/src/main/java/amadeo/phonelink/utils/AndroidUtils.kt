package amadeo.phonelink.utils

import android.content.Context
import android.content.Intent
import android.os.Build

val isRunningInEmulator: Boolean = Build.FINGERPRINT.startsWith("unknown")
        || Build.FINGERPRINT.startsWith("generic")
        || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))

        || Build.MODEL.contains("google_sdk")
        || Build.PRODUCT.contains("google_sdk")
        || Build.PRODUCT.contains("sdk_google")
        || Build.MODEL.contains("Emulator")
        || Build.MODEL.contains("Android SDK built for x86")
        || Build.PRODUCT.contains("sdk_x86")
        || Build.PRODUCT.contains("sdk")

        || Build.MODEL.toLowerCase().contains("droid4x")
        || Build.MANUFACTURER.contains("Genymotion")
        || Build.HARDWARE.contains("goldfish")
        || Build.HARDWARE.contains("ranchu")

        || Build.HARDWARE.contains("vbox86")
        || Build.PRODUCT.contains("vbox86p")

        || Build.PRODUCT.contains("emulator")
        || Build.PRODUCT.contains("simulator")

        || Build.BOARD.toLowerCase().contains("nox")
        || Build.BOOTLOADER.toLowerCase().contains("nox")
        || Build.HARDWARE.toLowerCase().contains("nox")
        || Build.PRODUCT.toLowerCase().contains("nox")

fun Context.closeNotificationDrawer(){
    this.sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
}
package amadeo.phonelink.device.connection

import amadeo.phonelink.BuildConfig
import amadeo.phonelink.utils.SecurityUtils
import amadeo.phonelink.device.*
import amadeo.phonelink.device.Device.Companion.KEY_LISTENING_TCP_PORT
import amadeo.phonelink.device.Device.Companion.KEY_TCP_PORT
import amadeo.phonelink.service.NetPackage
import amadeo.phonelink.service.UDPServer
import amadeo.phonelink.utils.isRunningInEmulator
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintWriter
import java.lang.Exception
import java.lang.ref.WeakReference
import java.net.InetSocketAddress
import java.net.SocketTimeoutException
import javax.net.ssl.*

private const val TAG = "PL PairingProcess"

const val PACKAGE_TYPE_IDENTITY = "identity"
const val PORT_CHECK_OK = "portCheckOk"

class PairingProcess(context: Context, val deviceManager: DeviceManager, val device: Device, val isClient: Boolean, val udpServer: UDPServer) :
    AsyncTask<Void, PairingProcess.Status, Pair<Boolean, Device>>() {
    companion object {
        const val PACKAGE_TYPE_PAIR_REQUEST = "pairRequest"
    }

    enum class Status {
        Initializing,
        WaitingForAccept,
        Pairing,
        Finished,
        Error
    }

    interface OnUpdateListener {
        fun onUpdate(status: Status)
    }

    interface OnFinishedListener {
        fun onFinished(success: Boolean, device: Device)
    }

    var updateListener: OnUpdateListener = object : OnUpdateListener {
        override fun onUpdate(status: Status) {}
    }
    var finishedListener: OnFinishedListener = object : OnFinishedListener {
        override fun onFinished(success: Boolean, device: Device) {}
    }

    private val contextWeakReference = WeakReference(context)

    private lateinit var sslContext: SSLContext
    private var sslServerSocket: SSLServerSocket? = null
    private var sslSocket: SSLSocket? = null

    private var printWriter: PrintWriter? = null
    private var bufferedReader: BufferedReader? = null

    override fun onPreExecute() {
        updateListener.onUpdate(Status.Initializing)
    }

    override fun doInBackground(vararg params: Void?): Pair<Boolean, Device>? {
        publishProgress(Status.WaitingForAccept)

        sslContext = SecurityUtils.getSSLContext(contextWeakReference.get()!!)

        try {
            if (isClient) {
                Log.d(TAG, "trying to connect to ${device.address} at ${device.tcpPort}")

                sslSocket = (sslContext.socketFactory.createSocket() as SSLSocket).apply {
                    connect(InetSocketAddress(device.address, device.tcpPort), 8000)
                    startHandshake()
                }

                setupNetworkConnection()
                publishProgress(Status.Pairing)

                Log.d(TAG, "before my")
                val myIdPackage = sendMyIdentityPackage()
                Log.d(TAG, "before remote")
                val remoteDeviceIdPackage = getRemoteDeviceIdentityPackage()
                Log.d(TAG, "after remote")

                remoteDeviceIdPackage.put(KEY_LISTENING_TCP_PORT, myIdPackage.getInt(KEY_TCP_PORT))

                val deviceCandidate: Device = Device.createFromNetPackage(remoteDeviceIdPackage)
                if (deviceCandidate == Device.InvalidDevice) {
                    Log.d(TAG, "device candidate is invalid")
                    publishProgress(Status.Error)
                    return Pair(false, deviceCandidate)
                }

                deviceCandidate.address = device.address;

                publishProgress(Status.Finished)
                return Pair(true, deviceCandidate)
            } else {
                Log.d(TAG, "sending pair request package")

                val connectionPort = if (BuildConfig.DEBUG && isRunningInEmulator) 6161 else deviceManager.getFreePort()
                val pairRequestPackage = DeviceUtils.getDeviceLookupResponsePackage().apply {
                    type = PACKAGE_TYPE_PAIR_REQUEST
                    put(KEY_TCP_PORT, connectionPort)
                }

                udpServer.sendNetPackage(
                    pairRequestPackage,
                    InetSocketAddress(device.address, 4848)
                ) //TODO("use constant from device discovery instead of 4848");

                Log.d(TAG, "creating server at $connectionPort")
                val socketFactory = sslContext.serverSocketFactory
                sslServerSocket = (socketFactory.createServerSocket(pairRequestPackage.getInt(KEY_TCP_PORT)) as SSLServerSocket).apply {
                    soTimeout = 8000
                }

                publishProgress(Status.WaitingForAccept)
                sslSocket = (sslServerSocket!!.accept() as SSLSocket).apply {
                    startHandshake()
                }

                setupNetworkConnection()
                publishProgress(Status.Pairing)

                Log.d(TAG, "before remote")
                val remoteDeviceIdPackage = getRemoteDeviceIdentityPackage()
                Log.d(TAG, "before my")
                val myIdPackage = sendMyIdentityPackage()
                Log.d(TAG, "after my")

                remoteDeviceIdPackage.put(KEY_LISTENING_TCP_PORT, myIdPackage.getInt(KEY_TCP_PORT))

                val deviceCandidate: Device = Device.createFromNetPackage(remoteDeviceIdPackage)
                if (deviceCandidate == Device.InvalidDevice) {
                    Log.d(TAG, "device candidate is invalid")
                    publishProgress(Status.Error)
                    return Pair(false, deviceCandidate)
                }

                deviceCandidate.address = device.address

                publishProgress(Status.Finished)
                return Pair(true, deviceCandidate)
            }
        } catch (e: SocketTimeoutException) {
            Log.d(TAG, "socket timeout exception")
            publishProgress(Status.Error)
        } catch (e: SSLHandshakeException) {
            Log.d(TAG, "ssl handshake exception")
            publishProgress(Status.Error)
        } catch (e: Exception) {
            publishProgress(Status.Error)
            e.printStackTrace()
        } finally {
            printWriter?.close()
            bufferedReader?.close()

            sslServerSocket?.close()
            sslSocket?.close()
        }

        return Pair(false, Device.InvalidDevice)
    }

    private fun setupNetworkConnection() {
        printWriter = PrintWriter(sslSocket!!.outputStream, true)
        bufferedReader = BufferedReader(InputStreamReader(sslSocket!!.inputStream))
    }

    private fun sendMyIdentityPackage(): NetPackage {
        var idPackage: NetPackage
        do {
            val port = deviceManager.getFreePort()
            idPackage = DeviceUtils.getDeviceIdentityPackage(port)
            printWriter?.println(idPackage.toString())
        } while (bufferedReader?.readLine() != PORT_CHECK_OK)

        return idPackage
    }

    private fun getRemoteDeviceIdentityPackage(): NetPackage {
        var idPackage: NetPackage
        while (true) {
            idPackage = NetPackage(serialized = bufferedReader?.readLine() ?: "")

            if (deviceManager.checkPortAvailability(idPackage.getInt(KEY_TCP_PORT))) {
                printWriter?.println(PORT_CHECK_OK)
                break
            } else
                printWriter?.println(" ")
        }

        return idPackage
    }

    override fun onProgressUpdate(vararg values: Status) {
        super.onProgressUpdate(*values)

        updateListener.onUpdate(values[0])
    }

    override fun onPostExecute(result: Pair<Boolean, Device>) {
        super.onPostExecute(result)

        finishedListener.onFinished(result.first, result.second)
    }

    override fun onCancelled() {
        printWriter?.close()
        bufferedReader?.close()

        sslServerSocket?.close()
        sslSocket?.close()
    }
}

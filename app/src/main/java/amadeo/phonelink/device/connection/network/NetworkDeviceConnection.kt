package amadeo.phonelink.device.connection.network

import amadeo.phonelink.utils.SecurityUtils
import amadeo.phonelink.device.Device
import amadeo.phonelink.device.Device.Companion.KEY_ID
import amadeo.phonelink.device.connection.DeviceConnection
import amadeo.phonelink.device.connection.network.DeviceConnectionWatchdog.Companion.KEY_WATCHDOG_ID
import amadeo.phonelink.device.connection.network.DeviceConnectionWatchdog.Companion.PACKAGE_TYPE_WATCHDOG_CONFIRMATION
import amadeo.phonelink.device.DeviceDiscovery
import amadeo.phonelink.service.IUDPNetPackageHandler
import amadeo.phonelink.service.NetPackage
import amadeo.phonelink.service.UDPServer
import amadeo.phonelink.utils.falseOrNull
import android.content.Context
import android.util.Base64
import android.util.Log
import java.io.*
import java.lang.ref.WeakReference
import java.net.ConnectException
import java.net.InetSocketAddress
import java.net.SocketException
import java.net.SocketTimeoutException
import java.nio.charset.StandardCharsets
import java.util.concurrent.LinkedBlockingDeque
import javax.net.ssl.SSLSocket

private const val TAG = "PL Device Connection"

class NetworkDeviceConnection(context: Context, val device: Device, private val udpServer: UDPServer) : DeviceConnection(),
    DeviceConnectionListener.OnNewConnectionListener,
    DevicePackageListener.OnNetPackageEventListener,
    DeviceConnectionWatchdog.OnConnectionBrokenListener {

    private val contextWeakReference = WeakReference(context)

    private val netPackages: LinkedBlockingDeque<NetPackage> = LinkedBlockingDeque()

    private var sslSocket: SSLSocket? = null
    private var connectionListener: DeviceConnectionListener? = null
    private var bufferedReader: BufferedReader? = null
    private var printWriter: PrintWriter? = null
    private var packageListener: DevicePackageListener? = null
    private var connectionWatchdog: DeviceConnectionWatchdog = DeviceConnectionWatchdog().apply {
        onConnectionBrokenListener = this@NetworkDeviceConnection
    }
    private var ipCheck: NetworkIPCheck = NetworkIPCheck(context, device, udpServer)

    private var running = true

    init {
        name = TAG
    }

    override fun sendNetPackage(netPackage: NetPackage) {
        if (!netPackage.has(KEY_WATCHDOG_ID))
            netPackage.put(KEY_WATCHDOG_ID, System.currentTimeMillis().toInt())

        netPackages.add(netPackage)
    }

    override fun run() {
        startConnectionListener()

        while (running) {
            var netPackage = NetPackage()

            try {
                Log.d(TAG, "taking package")
                netPackage = netPackages.take()

                if (sslSocket == null) {
                    ipCheck.perform()

                    Log.d(TAG, "socket is null, trying to connect to $device.id at ${device.address}")

                    val sslContext = SecurityUtils.getSSLContext(contextWeakReference.get()!!, device)
                    val sslSocket: SSLSocket = (sslContext.socketFactory.createSocket() as SSLSocket).apply {
                        connect(InetSocketAddress(device.address, device.listeningTcpPort), 1000)
                        startHandshake()
                    }

                    Log.d(TAG, "socket connected")
                    connectionListener?.interrupt()
                    onNewConnection(sslSocket)
                }

                Log.d(TAG, "sending ${netPackage.type}")
                printWriter?.println(netPackage.toString())
                connectionWatchdog.onSent(netPackage)
            } catch (e: InterruptedException) {
                Log.d(TAG, "take() interrupted")
                break
            } catch (e: ConnectException) {
                Log.d(TAG, "connect exception (${e.message}), ignoring package ${netPackage.type}")
                sslSocket = null

                if (ipCheck.onNetPackageFailed()) {
                    device.featureManager.onNetPackageFailed(netPackage)
                } else {
                    netPackages.addFirst(netPackage)
                }
            } catch (e: SocketTimeoutException) {
                Log.d(TAG, "socket timeout exception (${e.message}), ignoring package ${netPackage.type}")
                sslSocket = null

                if (ipCheck.onNetPackageFailed()) {
                    device.featureManager.onNetPackageFailed(netPackage)
                } else {
                    netPackages.addFirst(netPackage)
                }
            } catch (e: StreamCorruptedException) {
                Log.d(TAG, "stream corrupted, closing socket and trying again (${e.message}")
                sslSocket?.close()
                sslSocket = null

                netPackages.addFirst(netPackage)
            } catch (e: SocketException) {
                Log.d(TAG, "socket exception, closing socket and trying again (${e.message}")
                sslSocket?.close()
                sslSocket = null

                netPackages.addFirst(netPackage)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        connectionWatchdog.cleanup()

        sslSocket?.close()
        bufferedReader?.close()
        printWriter?.close()

        connectionListener?.interrupt()
        connectionListener?.join()

        packageListener?.interrupt()
        packageListener?.join()

        Log.d(TAG, "thread finished")
    }



    private fun startConnectionListener() {
        if (!running) {
            Log.d(TAG, "thread is not running, startConnectionListener() ignored")
            return
        }

        sslSocket?.close()
        sslSocket = null

        falseOrNull(connectionListener?.isAlive) { "startConnectionListener() should not be called when connection listener is already running" }

        connectionListener = DeviceConnectionListener(contextWeakReference.get()!!, device).apply {
            onNewConnectionListener = this@NetworkDeviceConnection
            start()
        }
    }

    override fun onNewConnection(sslSocket: SSLSocket) {
        Log.d(TAG, "preparing connection")
        this.sslSocket = sslSocket

        bufferedReader = BufferedReader(InputStreamReader(sslSocket.inputStream, StandardCharsets.UTF_8))
        printWriter = PrintWriter(OutputStreamWriter(sslSocket.outputStream, StandardCharsets.UTF_8), true)

        falseOrNull(packageListener?.isAlive) { "package listener should be dead" }

        packageListener = DevicePackageListener(bufferedReader!!).apply {
            netPackageEventListener = this@NetworkDeviceConnection
            start()
        }

        device.featureManager.onConnected()
    }

    override fun onNetPackageReceived(netPackage: NetPackage) {
        connectionWatchdog.onReceived(netPackage)

        if (netPackage.type != PACKAGE_TYPE_WATCHDOG_CONFIRMATION)
            sendNetPackage(NetPackage(PACKAGE_TYPE_WATCHDOG_CONFIRMATION).apply {
                put(KEY_WATCHDOG_ID, netPackage.getInt(KEY_WATCHDOG_ID))
            })

        device.featureManager.onNetPackageReceived(netPackage)
    }

    override fun onConnectionBroken() {
        sslSocket?.close()

        device.featureManager.onDisconnected()

        startConnectionListener()
    }

    override fun disconnect() {
        sslSocket?.close()
    }

    override fun interrupt() {
        super.interrupt()

        if (running) {
            running = false
            connectionWatchdog.cleanup()

            sslSocket?.close()
            bufferedReader?.close()
            printWriter?.close()

            connectionListener?.interrupt()
            connectionListener?.join()

            packageListener?.interrupt()
            packageListener?.join()
        }
    }
}

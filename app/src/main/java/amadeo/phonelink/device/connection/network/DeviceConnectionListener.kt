package amadeo.phonelink.device.connection.network

import amadeo.phonelink.utils.SecurityUtils
import amadeo.phonelink.device.Device
import android.content.Context
import android.util.Log
import java.lang.ref.WeakReference
import java.net.InetSocketAddress
import javax.net.ssl.SSLServerSocket
import javax.net.ssl.SSLSocket

private const val TAG = "PL DConnection Listener"

class DeviceConnectionListener(context: Context, val device: Device) : Thread() {

    init {
        name = TAG
    }

    interface OnNewConnectionListener {
        fun onNewConnection(sslSocket: SSLSocket)
    }

    var onNewConnectionListener = object : OnNewConnectionListener {
        override fun onNewConnection(sslSocket: SSLSocket) {}
    }

    private val contextWeakReference = WeakReference(context)
    private var running = true
    private var sslServerSocket: SSLServerSocket? = null


    override fun run() {
        Log.d(TAG, "starting at ${device.tcpPort}")

        try {
            val sslContext = SecurityUtils.getSSLContext(contextWeakReference.get()!!, device)
            val socketFactory = sslContext.serverSocketFactory
            sslServerSocket = (socketFactory.createServerSocket() as SSLServerSocket).apply {
                reuseAddress = true
                needClientAuth = true
                bind(InetSocketAddress(device.tcpPort))
            }

            val sslSocket: SSLSocket = sslServerSocket?.accept() as SSLSocket
            onNewConnectionListener.onNewConnection(sslSocket)
        } catch (e: Exception) {
            if (running)
                throw e
        } finally {
            sslServerSocket?.close()
        }

        Log.d(TAG, "thread finished")
    }

    override fun interrupt() {
        super.interrupt()

        if(running){
            running = false
            sslServerSocket?.close()
        }
    }
}

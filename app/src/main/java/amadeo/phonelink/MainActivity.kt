package amadeo.phonelink

import amadeo.phonelink.service.ForegroundService
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.iterator
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {
    companion object {
        const val ACTION_EXIT = "exit"
        const val ACTION_NAVIGATE = "switchFragment"

        const val ARG_NAVIGATE_DESTINATION = "destination"
        const val ARG_NAVIGATE_BUNDLE = "destinationBundle"
    }


    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navigationView: NavigationView
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        if (BuildConfig.DEBUG)
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

        //TODO("figure a way to make it look nicer instead of checking intent action twice")
        if (intent?.action != ACTION_EXIT) {
            setupService()
            setContentView(R.layout.activity_main)
            setupNavigation()
        }

        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun setupNavigation() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        navigationView = findViewById(R.id.nav_view)
        navController = findNavController(R.id.nav_host_fragment)

        val appBarConfiguration = AppBarConfiguration(getTopLevelDestinations(navigationView.menu), drawerLayout)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        navigationView.setupWithNavController(navController)

        //fix for activity title being set to default application name by onPostCreate() from activity class
        title = (navController.currentDestination?.label)
    }

    private fun setupService() {
        val serviceIntent = Intent(this, ForegroundService::class.java)
        startService(serviceIntent)
    }

    private fun getTopLevelDestinations(menu: Menu): Set<Int> {
        val destinations = mutableSetOf<Int>()
        for (menuItem: MenuItem in menu.iterator()) {
            if (menuItem.hasSubMenu()) {
                val subMenuDestinations = getTopLevelDestinations(menuItem.subMenu)
                destinations.addAll(subMenuDestinations)
            } else {
                destinations.add(menuItem.itemId)
            }
        }

        return destinations
    }

    private fun handleIntent(intent: Intent?) {
        if (intent == null || intent.action.isNullOrEmpty())
            return

        when (intent.action) {
            ACTION_EXIT -> {
                finishAndRemoveTask()
            }

            ACTION_NAVIGATE -> {
                if (intent.extras == null || intent.extras?.containsKey(ARG_NAVIGATE_DESTINATION) == false)
                    return

                val destination = intent.extras?.getInt(ARG_NAVIGATE_DESTINATION)
                navController.navigate(destination!!, intent.getBundleExtra(ARG_NAVIGATE_BUNDLE))
            }
        }

    }
}

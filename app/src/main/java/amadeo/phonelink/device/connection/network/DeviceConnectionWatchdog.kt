package amadeo.phonelink.device.connection.network

import amadeo.phonelink.service.NetPackage
import android.util.Log
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

private const val TAG = "PL DConnection Watchdog"
private const val DELAY = 5000

class DeviceConnectionWatchdog {
    companion object{
        const val PACKAGE_TYPE_WATCHDOG_CONFIRMATION = "watchdogConfirmation"
        const val KEY_WATCHDOG_ID = "watchdogId"
    }

    interface OnConnectionBrokenListener {
        fun onConnectionBroken()
    }

    var onConnectionBrokenListener = object : OnConnectionBrokenListener {
        override fun onConnectionBroken() {}
    }

    private var lastId: Int = 0
    private val receivedNetPackages: ArrayList<Int> = arrayListOf()

    private var watchdogCheckScheduledFeature: ScheduledFuture<Unit>? = null
    private val scheduledExecutorService: ScheduledExecutorService = Executors.newScheduledThreadPool(1)


    fun onSent(netPackage: NetPackage) {
        if (netPackage.type == PACKAGE_TYPE_WATCHDOG_CONFIRMATION)
            return

        lastId = netPackage.getInt(KEY_WATCHDOG_ID)

        watchdogCheckScheduledFeature?.cancel(true)
        scheduledExecutorService.schedule({
            Log.d(TAG, "check")
            if(lastId !in receivedNetPackages){
                Log.d(TAG, "check failed, required $lastId, received $receivedNetPackages")
                onConnectionBrokenListener.onConnectionBroken()
            }
            receivedNetPackages.clear()
        }, DELAY.toLong(), TimeUnit.MILLISECONDS)
    }

    fun onReceived(netPackage: NetPackage){
        receivedNetPackages.add(netPackage.getInt(KEY_WATCHDOG_ID))
    }


    fun cleanup() {
        scheduledExecutorService.shutdown()
    }
}
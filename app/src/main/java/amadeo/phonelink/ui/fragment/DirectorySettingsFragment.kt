package amadeo.phonelink.ui.fragment


import android.os.Bundle

import amadeo.phonelink.R
import amadeo.phonelink.ui.preference.AddStorageDirectoryPreference
import amadeo.phonelink.ui.preference.StorageDirectoryPreference
import android.net.Uri
import android.provider.DocumentsContract
import android.util.Log
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.Fragment
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat

private const val TAG = "PL SettingsContainerF"

class DirectorySettingsFragment : SettingsFragment(), DirectoryPickerDialogFragment.OnDirectorySelectedListener {

    private lateinit var directoriesCategory: PreferenceCategory

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences_directory_settings, rootKey)

        directoriesCategory = preferenceScreen.findPreference<PreferenceCategory>(getString(R.string.key_preference_category_settings_added_directories))!!

        val preference = StorageDirectoryPreference(preferenceManager.context).apply {
            title = "Test"
            summary = "summary"
        }

        directoriesCategory.addPreference(preference)
    }

    override fun onDisplayPreferenceDialog(preference: Preference?) {
        when (preference) {
            is AddStorageDirectoryPreference -> {
                val dialog = DirectoryPickerDialogFragment.newInstance(preference.key).apply {
                    setTargetFragment(this@DirectorySettingsFragment, 0)
                    onDirectorySelectedListener = this@DirectorySettingsFragment
                    //show(requireFragmentManager(), null)
                }
                dialog.show(requireFragmentManager(), null)
            }

            else -> super.onDisplayPreferenceDialog(preference)
        }
    }

    override fun onSelected(uri: Uri) {
        Log.d(TAG, "selected $uri")

        val preference = StorageDirectoryPreference(preferenceManager.context).apply {
            title = DocumentFile.fromTreeUri(requireContext(), uri)?.name
            summary = DocumentsContract.getTreeDocumentId(uri)
        }

        directoriesCategory.addPreference(preference)
    }
}

package amadeo.phonelink.service

import amadeo.phonelink.*
import amadeo.phonelink.device.*
import amadeo.phonelink.device.connection.PairingProcess
import amadeo.phonelink.ui.fragment.PairFragment
import amadeo.phonelink.utils.SecurityUtils
import amadeo.phonelink.utils.closeNotificationDrawer
import amadeo.phonelink.utils.runOnBackgroundThread
import android.app.*
import android.app.PendingIntent.FLAG_ONE_SHOT
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import java.net.InetSocketAddress

private const val TAG = "PL ForegroundService"

const val NOTIFICATION_CHANNEL_MAIN_ID = "main"
const val NOTIFICATION_MAIN_ID = 1

const val NOTIFICATION_CHANNEL_PAIR_ID = "pair"
const val NOTIFICATION_PAIR_ID = 2

class ForegroundService : Service(), PhoneLinkService, IUDPNetPackageHandler {

    companion object {
        const val ACTION_KILL_SERVICE = "kill"

        const val ACTION_ACCEPT_PAIR = "beginPair"
        const val ARG_ACCEPT_PAIR_BUNDLE = "pairFragmentBundle"

        const val ACTION_DECLINE_PAIR = "declinePair"
    }

    inner class PhoneLinkBinder : Binder() {
        fun getService(): ForegroundService {
            return this@ForegroundService
        }
    }

    lateinit var notificationManager: NotificationManager

    private val clearActionsNotificationExtender = NotificationCompat.Extender { builder ->
        builder.mActions.clear()
        builder
    }
    private lateinit var appNotificationBuilder: NotificationCompat.Builder
    private lateinit var pairNotificationBuilder: NotificationCompat.Builder

    private val binder = PhoneLinkBinder()

    override lateinit var udpServer: UDPServer
    override lateinit var deviceManager: DeviceManager
    override lateinit var pairingManager: PairingManager

    override val packageTypes: List<String> = listOf(PairingProcess.PACKAGE_TYPE_PAIR_REQUEST)

    override fun onCreate() {
        super.onCreate()

        SecurityUtils.init(this)

        prepareNotifications()

        startForeground(NOTIFICATION_MAIN_ID, appNotificationBuilder.build())

        udpServer = UDPServer().apply {
            addPackageHandler(this@ForegroundService)
            start()
        }
        deviceManager = DeviceManager(application, udpServer)
        pairingManager = PairingManager(this, deviceManager, udpServer)

        Log.d(TAG, "service started")
    }

    private fun prepareNotifications() {
        createNotificationChannels()
        createNotificationBuilders()
    }

    private fun createNotificationChannels() {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val appChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_MAIN_ID,
                getString(R.string.main_channel_name),
                NotificationManager.IMPORTANCE_DEFAULT
            )
            appChannel.description = getString(R.string.main_channel_description)

            val pairChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_PAIR_ID,
                getString(R.string.pair_channel_name),
                NotificationManager.IMPORTANCE_DEFAULT
            )
            pairChannel.description = getString(R.string.pair_channel_description)

            notificationManager.apply {
                createNotificationChannel(appChannel)
                createNotificationChannel(pairChannel)
            }
        }
    }

    private fun createNotificationBuilders() {

        val contentPendingIntent = PendingIntent.getActivity(this, 0, Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            action = MainActivity.ACTION_NAVIGATE
            addCategory(Intent.CATEGORY_LAUNCHER)
            putExtra(MainActivity.ARG_NAVIGATE_DESTINATION, R.id.nav_devices)
        }, 0)

        val killServicePendingIntent =
            PendingIntent.getService(this, 0, Intent(this, ForegroundService::class.java).apply {
                action = ACTION_KILL_SERVICE
            }, FLAG_ONE_SHOT)

        appNotificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_MAIN_ID)
            .setContentTitle(getText(R.string.main_notification_title))
            .setContentText(getText(R.string.main_notification_text))
            .setSmallIcon(R.drawable.ic_devices)
            .setContentIntent(contentPendingIntent)
            .addAction(R.drawable.ic_decline, getText(R.string.close_app), killServicePendingIntent)

        pairNotificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_PAIR_ID)
            .setContentTitle(getText(R.string.pair_notification_title))
            .setContentText(getText(R.string.pair_notification_text))
            .setSmallIcon(R.drawable.ic_pair)

    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null && intent.action != null) {
            when (intent.action) {
                ACTION_KILL_SERVICE -> {
                    val closeIntent = Intent(this, MainActivity::class.java).apply {
                        action = MainActivity.ACTION_EXIT
                        addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    }
                    startActivity(closeIntent)

                    stopForeground(true)
                    stopSelf()
                }

                ACTION_ACCEPT_PAIR -> {
                    startActivity(Intent(this, MainActivity::class.java).apply {
                        action = MainActivity.ACTION_NAVIGATE
                        addCategory(Intent.CATEGORY_LAUNCHER)
                        addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                        putExtra(MainActivity.ARG_NAVIGATE_DESTINATION, R.id.nav_pair)
                        putExtra(MainActivity.ARG_NAVIGATE_BUNDLE, intent.getBundleExtra(ARG_ACCEPT_PAIR_BUNDLE))
                    })

                    notificationManager.cancel(NOTIFICATION_PAIR_ID)
                    closeNotificationDrawer()
                }

                ACTION_DECLINE_PAIR -> {
                    notificationManager.cancel(NOTIFICATION_PAIR_ID)
                    closeNotificationDrawer()
                }
            }
        }

        return START_STICKY
    }

    override fun handleUDPNetPackage(netPackage: NetPackage, inetSocketAddress: InetSocketAddress): Boolean {
        return when (netPackage.type) {
            PairingProcess.PACKAGE_TYPE_PAIR_REQUEST -> {
                Log.d(TAG, "pair request package received from ${inetSocketAddress.address}")
                val device = Device.createFromNetPackage(netPackage)
                if (device != Device.InvalidDevice) {
                    if (deviceManager.isPaired(device)) {
                        Log.d(TAG, "received pair request from ${device.id} but the device is already paired")
                        return true
                    }

                    netPackage.put(Device.KEY_ADDRESS, inetSocketAddress.address.hostAddress)

                    val acceptPairIntent = PendingIntent.getService(
                        this@ForegroundService, 0,
                        Intent(this@ForegroundService, ForegroundService::class.java).apply {
                            action = ACTION_ACCEPT_PAIR
                            putExtra(ARG_ACCEPT_PAIR_BUNDLE, PairFragment.createBundle(netPackage))
                        }, FLAG_ONE_SHOT
                    )

                    val declinePairIntent = PendingIntent.getService(
                        this@ForegroundService, 0,
                        Intent(this@ForegroundService, ForegroundService::class.java).apply {
                            action = ACTION_DECLINE_PAIR
                        }, FLAG_ONE_SHOT
                    )

                    val pairNotification = pairNotificationBuilder
                        .extend(clearActionsNotificationExtender)
                        .setContentText(getString(R.string.pair_notification_text, device.id))
                        .addAction(R.drawable.ic_accept, getText(R.string.accept), acceptPairIntent)
                        .addAction(R.drawable.ic_decline, getText(R.string.decline), declinePairIntent)
                        .build()

                    notificationManager.notify(NOTIFICATION_PAIR_ID, pairNotification)
                }

                true
            }

            else -> {
                false
            }
        }
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")

        runOnBackgroundThread({
            pairingManager.cleanup()
            deviceManager.cleanup()

            udpServer.interrupt()
            udpServer.join()
        }, true)

        Log.d(TAG, "service done")
    }
}
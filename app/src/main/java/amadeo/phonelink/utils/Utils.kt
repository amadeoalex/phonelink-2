package amadeo.phonelink.utils

/**
 * Throws an [IllegalStateException] with the result of calling [lazyMessage] if the [value] is not null and true.
 */
inline fun falseOrNull(value: Boolean?, lazyMessage: () -> Any) {
    if (value != null && value) {
        val message = lazyMessage()
        throw IllegalStateException(message.toString())
    }
}
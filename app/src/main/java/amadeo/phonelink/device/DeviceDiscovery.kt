package amadeo.phonelink.device

import amadeo.phonelink.BuildConfig
import amadeo.phonelink.utils.NetUtils
import amadeo.phonelink.device.connection.DeviceConnection
import amadeo.phonelink.device.types.Phone
import amadeo.phonelink.service.IUDPNetPackageHandler
import amadeo.phonelink.service.NetPackage
import amadeo.phonelink.service.UDPServer
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import java.lang.ref.WeakReference
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.ClosedByInterruptException
import java.nio.channels.DatagramChannel
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

private const val TAG = "PL DeviceDiscovery"

class DeviceDiscovery(private val udpServer: UDPServer) : IUDPNetPackageHandler {
    companion object {
        const val PACKAGE_TYPE_LOOKUP = "lookup"
        const val PACKAGE_TYPE_LOOKUP_RESPONSE = "lookupResponse"
    }

    override val packageTypes: List<String> = listOf(
        PACKAGE_TYPE_LOOKUP,
        PACKAGE_TYPE_LOOKUP_RESPONSE
    )

    private var stopScanScheduledFeature: ScheduledFuture<Unit>? = null
    private val scheduledExecutorService: ScheduledExecutorService = Executors.newScheduledThreadPool(2)
    private var internalFoundDevices: ArrayList<Device> = ArrayList()

    var isScanning: Boolean = false
        set(value) {
            if (field != value) {
                field = value
                scanning.postValue(value)
            }
        }
    val scanning: MutableLiveData<Boolean> = MutableLiveData(isScanning)
    val foundDevices: MutableLiveData<ArrayList<Device>> = MutableLiveData()

    init {
//        foundDevices.postValue(ArrayList<Device>().also {
//            it.add(
//                Phone(
//                    "id1",
//                    byteArrayOf(),
//                    "127.0.0.1",
//                    0,
//                    0,
//                    "",
//                    "",
//                    DeviceConnection.NoConnection
//                )
//            )
//            it.add(
//                Phone(
//                    "id2",
//                    byteArrayOf(),
//                    "127.0.0.1",
//                    0,
//                    0,
//                    "",
//                    "",
//                    DeviceConnection.NoConnection
//                )
//            )
//        })

        udpServer.addPackageHandler(this)
    }

    override fun handleUDPNetPackage(netPackage: NetPackage, inetSocketAddress: InetSocketAddress): Boolean {
        return when (netPackage.type) {
            PACKAGE_TYPE_LOOKUP -> {
                Log.d(TAG, "lookup package received from ${inetSocketAddress.address}")
                udpServer.sendNetPackage(DeviceUtils.getDeviceLookupResponsePackage(), inetSocketAddress)

                true
            }

            PACKAGE_TYPE_LOOKUP_RESPONSE -> {
                Log.d(TAG, "lookup response package received from ${inetSocketAddress.address}")
                if (isScanning) {
                    val device = Device.createFromNetPackage(netPackage)
                    if (device != Device.InvalidDevice) {
                        if (device !in internalFoundDevices) {
                            internalFoundDevices.add((device).apply {
                                address = inetSocketAddress.address.hostAddress
                            })
                            foundDevices.postValue(internalFoundDevices)
                        }
                    } else
                        Log.d(TAG, "error creating device from package $netPackage")
                }

                true
            }

            else -> {
                false
            }
        }
    }

    @Synchronized
    fun startScan(milliseconds: Long = 5 * 1000) {
        internalFoundDevices.clear()
        stopScanScheduledFeature?.cancel(true)
        isScanning = true

        scheduledExecutorService.execute {
            for (address in NetUtils.getBroadcastAddresses()) {
                udpServer.sendNetPackage(DeviceUtils.getDeviceLookupPackage(), InetSocketAddress(address, 4848))
            }

            if (BuildConfig.DEBUG)
                udpServer.sendNetPackage(DeviceUtils.getDeviceLookupPackage(), InetSocketAddress("10.0.2.2", 4848))
        }

        scheduledExecutorService.schedule({
            isScanning = false
            foundDevices.postValue(internalFoundDevices)
        }, milliseconds, TimeUnit.MILLISECONDS)
    }

    @Synchronized
    fun stopScan() {
        stopScanScheduledFeature?.cancel(true)
        isScanning = false

        foundDevices.postValue(internalFoundDevices)
    }

    fun cleanup() {
        scheduledExecutorService.shutdown()
    }
}
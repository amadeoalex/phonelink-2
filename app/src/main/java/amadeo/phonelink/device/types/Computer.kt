package amadeo.phonelink.device.types

import amadeo.phonelink.device.Device
import amadeo.phonelink.device.connection.DeviceConnection

class Computer(
    override var id: String,
    override var certificate: ByteArray,
    override var address: String,
    override var tcpPort: Int,
    override var listeningTcpPort: Int,
    override var ssid: String,
    override var bSSID: String,
    override var connection: DeviceConnection
) : Device() {

    override var type: String = TYPE_PC

    constructor() : this(
        "",
        byteArrayOf(),
        "0.0.0.0",
        0,
        0,
        "",
        "",
        DeviceConnection.NoConnection
    )

}
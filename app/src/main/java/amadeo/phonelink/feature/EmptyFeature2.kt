package amadeo.phonelink.feature

import amadeo.phonelink.device.Device
import amadeo.phonelink.service.NetPackage
import android.content.Context
import android.util.Log

class EmptyFeature2(override val device: Device) : Feature() {
    override val TAG = "PL EmptyFeature2"
    override val key = "feature_empty2"
    override val netPackagePrefix = "ef2_"

    override val displayName = "Empty Feature2"

    override fun onEnabled(context: Context) {
        super.onEnabled(context)
        Log.d(TAG, getTagExtension() + "started")
    }

    override fun onDisabled(context: Context) {
        super.onDisabled(context)
        Log.d(TAG, getTagExtension() + "stopped")
    }

    override fun onConnected() {

    }

    override fun onNetPackageReceived(netPackage: NetPackage) {

    }

    override fun onNetPackageFailed(netPackage: NetPackage) {

    }

    override fun onDisconnected() {

    }

}
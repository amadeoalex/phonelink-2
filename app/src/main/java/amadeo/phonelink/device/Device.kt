package amadeo.phonelink.device

import amadeo.phonelink.device.connection.DeviceConnection
import amadeo.phonelink.device.types.Computer
import amadeo.phonelink.device.types.Phone
import amadeo.phonelink.feature.IFeatureManager
import amadeo.phonelink.service.NetPackage
import org.json.JSONObject

abstract class Device {
    companion object {
        const val KEY_ID: String = "id"
        const val KEY_TYPE = "type"
        const val KEY_CERTIFICATE = "certificate"
        const val KEY_ADDRESS = "address"
        const val KEY_TCP_PORT = "tcpPort"
        const val KEY_LISTENING_TCP_PORT = "listeningTcpPort"
        const val KEY_BSSID = "bSSID"
        const val KEY_SSID = "ssid"

        const val TYPE_INVALID = "invalidDevice"
        const val TYPE_PHONE = "phone"
        const val TYPE_TABLET = "tablet"
        const val TYPE_PC = "computer"
        const val TYPE_LAPTOP = "laptop"


        val InvalidDevice = object : Device() {
            override var id: String = TYPE_INVALID
            override var type = TYPE_INVALID
            override var certificate: ByteArray = byteArrayOf()
            override var address: String = TYPE_INVALID
            override var tcpPort: Int = -1
            override var listeningTcpPort: Int = -1
            override var ssid: String = TYPE_INVALID
            override var bSSID: String = TYPE_INVALID
        }

        fun createFromJSONObject(jsonObject: JSONObject): Device {
            val deviceType = jsonObject.optString(KEY_TYPE, TYPE_INVALID)

            val device: Device = when (deviceType) {
                TYPE_PC -> Computer()
                TYPE_PHONE -> Phone()
                else -> InvalidDevice
            }

            device.initializeFromJSONObject(jsonObject)

            return device
        }

        fun createFromNetPackage(netPackage: NetPackage): Device = createFromJSONObject(netPackage)
    }

    abstract var id: String
    abstract var type: String
    abstract var certificate: ByteArray
    abstract var address: String
    abstract var tcpPort: Int
    abstract var listeningTcpPort: Int
    abstract var ssid: String
    abstract var bSSID: String

    open var connection: DeviceConnection = DeviceConnection.NoConnection
    open var featureManager: IFeatureManager = IFeatureManager.NoFeatureManager

    fun initializeFromJSONObject(jsonObject: JSONObject) {
        id = jsonObject.optString(KEY_ID, id)
        certificate = if (jsonObject.has(KEY_CERTIFICATE))
            android.util.Base64.decode(jsonObject.getString(KEY_CERTIFICATE), android.util.Base64.DEFAULT)
        else
            certificate

        address = jsonObject.optString(KEY_ADDRESS, address)
        tcpPort = jsonObject.optInt(KEY_TCP_PORT, tcpPort)
        listeningTcpPort = jsonObject.optInt(KEY_LISTENING_TCP_PORT, listeningTcpPort)
        ssid = jsonObject.optString(KEY_SSID, ssid)
        bSSID = jsonObject.optString(KEY_BSSID, bSSID)
    }

    fun toJSONObject(): JSONObject {
        return JSONObject().apply {
            put(KEY_ID, id)
            put(KEY_TYPE, type)
            put(KEY_CERTIFICATE, android.util.Base64.encodeToString(certificate, android.util.Base64.DEFAULT))
            put(KEY_ADDRESS, address)
            put(KEY_TCP_PORT, tcpPort)
            put(KEY_LISTENING_TCP_PORT, listeningTcpPort)
            put(KEY_BSSID, bSSID)
            put(KEY_SSID, ssid)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Device)
            return false

        if (other === this)
            return true

        //TODO("compare tcp ports - if pairing requested from remote device fails, clicking on it begins pairing on port 0 "PairingProcess: trying to connect to 192.168.1.100 at 0"")
        return other.id == this.id &&
                other.type == this.type &&
                other.certificate.contentEquals(this.certificate)
    }

    override fun toString(): String {
        return toJSONObject().toString()
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + certificate.contentHashCode()
        return result
    }
}





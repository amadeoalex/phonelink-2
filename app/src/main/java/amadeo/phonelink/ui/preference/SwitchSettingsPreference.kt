package amadeo.phonelink.ui.preference

import amadeo.phonelink.R
import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.CompoundButton
import android.widget.Switch
import androidx.preference.*

class SwitchSettingsPreference(context: Context?, attrs: AttributeSet?) :
    SwitchPreference(context, attrs) {

    constructor(context: Context?) : this(context, null)

    init {
        widgetLayoutResource = R.layout.preference_widget_switch_settings
    }

    @SuppressLint("InlinedApi")
    override fun onBindViewHolder(holder: PreferenceViewHolder?) {
        super.onBindViewHolder(holder)

        if (holder == null)
            return

        val switch = holder.findViewById(android.R.id.switch_widget) as Switch
        switch.setOnClickListener {}
    }

    override fun onClick() {}
}
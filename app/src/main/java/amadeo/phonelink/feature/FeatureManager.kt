package amadeo.phonelink.feature

import amadeo.phonelink.device.Device
import amadeo.phonelink.device.DeviceManager
import amadeo.phonelink.feature.dummy.DummyFeature
import amadeo.phonelink.service.NetPackage
import amadeo.phonelink.utils.*
import android.content.Context

private const val APPENDIX_ENABLED = "_enabled"

class FeatureManager(val device: Device) : IFeatureManager {

    private lateinit var settingsOwner: PreferenceSettingsOwner
    private lateinit var settings: PreferenceSettings

    private val features: HashMap<String, Feature> = hashMapOf()
    private val enabledFeatures: HashMap<String, Feature> = hashMapOf()

    override val featureList: List<Feature>
        get() {
            return features.values.toList()
        }

    override val enabledFeatureList: List<Feature>
        get() {
            return enabledFeatures.values.toList()
        }

    override fun initialize(context: Context) {
        settingsOwner = PreferenceSettingsOwner(device::class.java, device.id)
        settings = context.getPreferenceSettings(settingsOwner)
    }

    init {
        addFeature(DummyFeature(device))
        addFeature(EmptyFeature1(device))
        addFeature(EmptyFeature2(device))
        addFeature(EmptyFeature3(device))
    }

    private fun addFeature(feature: Feature) {
        features[feature.key] = feature
    }

    override fun startEnabledFeatures(context: Context) {
        synchronized(features) {
            features.forEach { (key, feature) ->
                if (settings[PreferenceSetting(feature.key + APPENDIX_ENABLED, true)]) {
                    synchronized(enabledFeatures) { enabledFeatures[key] = feature }
                    feature.onEnabled(context)
                }
            }
        }
    }

    override fun setFeatureEnabled(context: Context, key: String, enabled: Boolean) {
        if (enabled)
            enableFeature(context, key)
        else
            disableFeature(context, key)
    }

    fun enableFeature(context: Context, key: String) {
        if (!features.containsKey(key) || enabledFeatures.containsKey(key))
            return

        settings[PreferenceSetting(key + APPENDIX_ENABLED, true)] = true

        synchronized(enabledFeatures) {
            val feature: Feature = features[key]!!
            enabledFeatures[key] = feature
            feature.onEnabled(context)
        }
    }

    fun disableFeature(context: Context, key: String) {
        if (!features.containsKey(key) || !enabledFeatures.containsKey((key)))
            return

        settings[PreferenceSetting(key + APPENDIX_ENABLED, true)] = false

        synchronized(enabledFeatures) {
            val feature: Feature = features[key]!!
            feature.onDisabled(context)
            enabledFeatures.remove(key)
        }
    }


    override fun getFeature(key: String): Feature? {
        return features[key]
    }

    override fun onConnected() {
        synchronized(enabledFeatures) {
            enabledFeatures.values.forEach {
                it.onConnected()
            }
        }
    }

    override fun onNetPackageReceived(netPackage: NetPackage) {
        synchronized(enabledFeatures) {
            enabledFeatures.values.forEach {
                if (netPackage.type.startsWith(it.netPackagePrefix))
                    it.onNetPackageReceived(netPackage)
            }
        }
    }

    override fun onNetPackageFailed(netPackage: NetPackage) {
        synchronized(enabledFeatures) {
            enabledFeatures.values.forEach {
                if (netPackage.type.startsWith(it.netPackagePrefix))
                    it.onNetPackageFailed(netPackage)
            }
        }
    }

    override fun onDisconnected() {
        synchronized(enabledFeatures) {
            enabledFeatures.values.forEach {
                it.onDisconnected()
            }
        }
    }

    override fun cleanup(context: Context) {
        context.savePreferenceSettings(settingsOwner, settings)

        synchronized(enabledFeatures) {
            enabledFeatures.values.forEach {
                it.onDisabled(context)
            }
        }
    }

}
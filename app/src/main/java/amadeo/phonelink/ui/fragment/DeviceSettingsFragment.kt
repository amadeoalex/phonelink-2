package amadeo.phonelink.ui.fragment

import amadeo.phonelink.R
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.*
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat

private const val TAG = "PL DeviceSettingsF"

class DeviceSettingsFragment : SettingsFragment() {

    companion object {
        const val ARG_DEVICE_ID = "deviceId"

        fun createBundle(deviceId: String): Bundle {
            return Bundle().apply {
                putString(ARG_DEVICE_ID, deviceId)
            }
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        Toast.makeText(context, "device id " + arguments?.getString(ARG_DEVICE_ID), Toast.LENGTH_SHORT).show()
        setPreferencesFromResource(R.xml.preferences_device_settings, rootKey)

        title = getString(R.string.device_settings, arguments?.getString(ARG_DEVICE_ID))
    }
}
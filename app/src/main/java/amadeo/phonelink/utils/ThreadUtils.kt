package amadeo.phonelink.utils

import android.os.Looper

fun runOnBackgroundThread(action: () -> Unit, join: Boolean) {
    if (Looper.getMainLooper().isCurrentThread)
        Thread { action() }.apply {
            start()
            if (join) join()
        }
    else
        action()
}
package amadeo.phonelink.device

import amadeo.phonelink.utils.JSONUtils
import amadeo.phonelink.device.connection.network.NetworkDeviceConnection
import amadeo.phonelink.device.connection.DeviceConnection
import amadeo.phonelink.device.types.Phone
import amadeo.phonelink.feature.FeatureManager
import amadeo.phonelink.service.UDPServer
import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import android.util.Log
import androidx.lifecycle.MutableLiveData
import org.json.JSONArray
import java.lang.ref.WeakReference
import kotlin.random.Random

private const val TAG = "PL DeviceManager"
private const val KEY_SERIALIZED_DEVICES = "devices"

class DeviceManager(context: Context, private val udpServer: UDPServer) {

    private val contextWeakReference: WeakReference<Context> = WeakReference(context)

    private val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private var internalDevices: ArrayList<Device> = arrayListOf()

    private val random = Random(System.currentTimeMillis())

    var devices: MutableLiveData<List<Device>> = MutableLiveData()

    val deviceDiscovery: DeviceDiscovery = DeviceDiscovery(udpServer)

    init {
        deserializeDevices()
    }

    private fun deserializeDevices() {
        val serializedDevices = preferences.getString(KEY_SERIALIZED_DEVICES, "[]")
        val devicesJSONArray = JSONUtils.verifyJSONArray(serializedDevices!!)
        if (devicesJSONArray == JSONUtils.InvalidJSONObject) {
            Log.w(TAG, "devices json is corrupted")
            preferences.edit().apply {
                putString(KEY_SERIALIZED_DEVICES, "[]")
                apply()
            }
        } else {
            for (i in 0 until devicesJSONArray.length()) {
                val deviceJSON = devicesJSONArray.getJSONObject(i)
                val device: Device = Device.createFromJSONObject(deviceJSON)

                if (device == Device.InvalidDevice) {
                    Log.w(TAG, "device $deviceJSON is corrupted")
                    continue
                }

                add(device, false)
            }
        }

        add(Phone("Dummy phone", byteArrayOf(), "123.456.0.1", 1234, 4321, "ssid", "bssid", DeviceConnection.NoConnection).apply { })
        add(Phone("Other dummy phone", byteArrayOf(), "123.456.0.15", 1111, 5555, "ssid2", "bssid2", DeviceConnection.NoConnection))


        this.devices.postValue(internalDevices)
    }

    private fun initializeDevice(device: Device) {
        if (device.certificate.isNotEmpty()) {
            device.apply {
                connection = NetworkDeviceConnection(contextWeakReference.get() as Context, device, udpServer).apply {
                    start()
                }
                featureManager = FeatureManager(device).apply {
                    initialize(contextWeakReference.get() as Context)
                    startEnabledFeatures(contextWeakReference.get() as Context)
                }
            }
        } else
            Log.d(TAG, "device ${device.id} is missing a certificate, skipping initialization")
    }

    @Synchronized
    fun checkPortAvailability(port: Int): Boolean {
        for (device in internalDevices) {
            if (device.tcpPort == port || device.listeningTcpPort == port)
                return false
        }

        return true
    }

    @Synchronized
    fun getFreePort(): Int {
        var port: Int
        do {
            port = random.nextInt(5555, 9999)
        } while (!checkPortAvailability(port))

        return port

    }

    @Synchronized
    fun isPaired(device: Device): Boolean {
        return device in internalDevices
    }

    @Synchronized
    fun isDeviceWithIdPaired(deviceId: String): Boolean {
        return internalDevices.firstOrNull { device -> device.id == deviceId } != null
    }

    @Synchronized
    private fun add(device: Device, postValue: Boolean) {
        if (device == Device.InvalidDevice)
            return

        if (isPaired(device))
            return

        internalDevices.add(device)
        initializeDevice(device)

        if (postValue)
            devices.postValue(internalDevices)
    }

    @Synchronized
    fun add(device: Device) {
        add(device, true)
    }

    @Synchronized
    fun remove(device: Device) {
        if (!isPaired(device))
            return

        //TODO("consider deinitialization in this place")
        internalDevices.remove(device)
        devices.postValue(internalDevices)
    }

    @Synchronized
    fun get(deviceId: String): Device {
        //TODO("currently there may be situation where two devices share the same id - add display name
        // and assign id based on the model number and uid provided by api29")
        return internalDevices.first { device -> device.id == deviceId }
    }

    fun cleanup() {
        deviceDiscovery.cleanup()

        synchronized(internalDevices) {
            val serializedDevices = JSONArray()
            for (device in internalDevices) {
                if (device.certificate.isEmpty())
                    continue

                device.connection.apply {
                    interrupt()
                    join()
                }

                device.featureManager.cleanup(contextWeakReference.get()!!)

                serializedDevices.put(device.toJSONObject())
            }

            preferences.edit().apply {
                putString(KEY_SERIALIZED_DEVICES, serializedDevices.toString())
                apply()
            }
        }
    }
}

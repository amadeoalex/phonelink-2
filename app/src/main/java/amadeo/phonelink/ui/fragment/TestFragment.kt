package amadeo.phonelink.ui.fragment


import amadeo.phonelink.R
import amadeo.phonelink.service.NetPackage
import amadeo.phonelink.vmodel.NotifyingAndroidServiceViewModel
import amadeo.phonelink.vmodel.TestViewModel
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.ToggleButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider

class TestFragment : Fragment(), NotifyingAndroidServiceViewModel.OnViewModelReadyListener {

    lateinit var testViewModel: TestViewModel
    lateinit var testButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_test, container, false)

        testViewModel = ViewModelProvider(activity!!)[TestViewModel::class.java]

        val deviceId: TextView = view.findViewById(R.id.device_id)
        val deviceIdMore: TextView = view.findViewById(R.id.device_id_more)
        val deviceAddress: TextView = view.findViewById(R.id.device_address)
        val deviceTcpPort: TextView = view.findViewById(R.id.device_tcp_port)
        val deviceListeningTcpPort: TextView = view.findViewById(R.id.device_listening_tcp_port)

        val expand: ToggleButton = view.findViewById(R.id.button_expand)
        val moreLayout: ConstraintLayout = view.findViewById(R.id.device_more_layout)

        expand.setOnClickListener {
            moreLayout.visibility = if (expand.isChecked) View.VISIBLE else View.GONE
            Log.d("PL TEST", moreLayout.visibility.toString())
            //view.requestLayout()
        }

        testButton = view.findViewById(R.id.test_button)
        testViewModel.setOnReadyListener(this)

        return view
    }

    override fun onViewModelReady() {
        testButton.setOnClickListener{
            testViewModel.deviceManager.devices.value?.forEach {
                it.connection.sendNetPackage(NetPackage("somepackage"))
            }
        }
    }
}

package amadeo.phonelink.ui.fragment


import amadeo.phonelink.R
import amadeo.phonelink.device.Device
import amadeo.phonelink.device.connection.PairingProcess
import amadeo.phonelink.service.NetPackage
import amadeo.phonelink.ui.adapter.DeviceAdapter
import amadeo.phonelink.ui.adapter.ScanResultAdapter
import amadeo.phonelink.vmodel.NotifyingAndroidServiceViewModel
import amadeo.phonelink.vmodel.PairingViewModel
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

private const val TAG = "PL PairFragment"

class PairFragment : Fragment(), ScanResultAdapter.OnItemClickedListener {
    companion object {
        const val ARG_DEVICE_NET_PACKAGE = "netPackage"

        fun createBundle(deviceNetPackage: NetPackage): Bundle {
            return Bundle().apply {
                putString(ARG_DEVICE_NET_PACKAGE, deviceNetPackage.toString())
            }
        }
    }

    private lateinit var viewModel: PairingViewModel

    private lateinit var deviceAdapter: ScanResultAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var refreshLayout: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_pair, container, false)

        arguments

        viewModel = ViewModelProvider(activity!!)[PairingViewModel::class.java]

        deviceAdapter = ScanResultAdapter().apply {
            itemClickedListener = this@PairFragment
        }

        viewManager = LinearLayoutManager(activity)

        recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view).apply {
            layoutManager = viewManager
            adapter = deviceAdapter
        }

        refreshLayout = view.findViewById(R.id.refresh_layout)
        refreshLayout.setOnRefreshListener { viewModel.deviceManager.deviceDiscovery.startScan() }

        viewModel.setOnReadyListener(object : NotifyingAndroidServiceViewModel.OnViewModelReadyListener {
            override fun onViewModelReady() {
                setupObserved()

                if (arguments?.containsKey(ARG_DEVICE_NET_PACKAGE) == true)
                    pairToProvidedDevice()
                else
                    viewModel.deviceManager.deviceDiscovery.startScan()
            }
        })

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_pair_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.refresh -> {
                viewModel.deviceManager.deviceDiscovery.startScan()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupObserved() {
        viewModel.deviceManager.deviceDiscovery.foundDevices.observe(this@PairFragment, Observer<ArrayList<Device>> { devices ->
            deviceAdapter.submitList(ArrayList(devices))
        })

        viewModel.deviceManager.deviceDiscovery.scanning.observe(this@PairFragment, Observer<Boolean> { scanning ->
            refreshLayout.isRefreshing = scanning
        })
    }

    private fun pairToProvidedDevice() {
        val deviceNetPackage = arguments?.getString(ARG_DEVICE_NET_PACKAGE)
        if (deviceNetPackage == null) {
            Log.w(TAG, "device net package is null")
            return
        }

        val device = Device.createFromNetPackage(NetPackage(serialized = deviceNetPackage))
        if (device == Device.InvalidDevice) {
            Log.w(TAG, "created device if invalid")
            return
        }

        deviceAdapter.add(0, device)
        recyclerView.postDelayed({
            (recyclerView.findViewHolderForAdapterPosition(0) as ScanResultAdapter.ScanResultHolder).apply {
                isClient = true
                itemView.performClick()
            }
        }, 50)
    }

    override fun onItemClicked(device: Device, holder: ScanResultAdapter.ScanResultHolder) {
        if (viewModel.deviceManager.isDeviceWithIdPaired(device.id)) {
            holder.pairingStatus.visibility = View.VISIBLE
            holder.pairingStatus.setText(R.string.device_with_id_already_paired)
            return
        }

        val pairingProcess = viewModel.pairingManager.getPairingProcess(device, holder.isClient)
        if (pairingProcess.status == AsyncTask.Status.PENDING) {
            holder.pairingStatus.visibility = View.VISIBLE
            holder.progressBar.visibility = View.VISIBLE
            pairingProcess.apply {
                updateListener = object : PairingProcess.OnUpdateListener {
                    override fun onUpdate(status: PairingProcess.Status) {
                        holder.pairingStatus.setText(statusToTextId(status))
                    }
                }
                finishedListener = object : PairingProcess.OnFinishedListener {
                    override fun onFinished(success: Boolean, device: Device) {
                        holder.progressBar.visibility = View.GONE
                        if (success)
                            deviceManager.add(device)
                    }

                }
            }
            pairingProcess.execute()
        }
    }

    private fun statusToTextId(status: PairingProcess.Status): Int {
        return when (status) {
            PairingProcess.Status.Initializing -> R.string.initializing
            PairingProcess.Status.WaitingForAccept -> R.string.waiting_for_accept
            PairingProcess.Status.Pairing -> R.string.pairing
            PairingProcess.Status.Finished -> R.string.finished
            PairingProcess.Status.Error -> R.string.error
        }
    }
}
